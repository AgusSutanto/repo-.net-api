﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using GardaMedikaReactAPI.Repository;
using GardaMobile.Framework.API;
using GardaMobile.Framework.API.Handlers;


namespace GardaMedikaReactAPI.App_Start
{
    public class WebAPIConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "StandardApi",
                routeTemplate: "{controller}/{action}"
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{version}/{controller}/{action}"
            );

            // always place logging handler at the last order
            //config.MessageHandlers.Clear();
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["IsLoggingEnabled"]))
            //    config.MessageHandlers.Add(new LoggingHandler());

            config.Services.Replace(typeof(IHttpControllerSelector), new VersioningHttpControllerSelector(config));
        }
    }
}