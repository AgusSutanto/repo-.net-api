﻿using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Repository.v1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using static GardaMedikaReactAPI.Model.v1.SEAObject;
using GardaMedikaReactAPI.Model.v1;
using System.IO;
using System.Security;
using System.Net.Http;

namespace GardaMedikaReactAPI.Controller.v1
{
    public class GardaMedikaController : GardaMedikaBaseController
    {
        [HttpPost, Route("GetTreatmentSymptomsList")]
        public IHttpActionResult GetTreatmentSymptomsList(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string languageCode = form.Get("LangCode");
                    List<Symptom> result = GardaMedikaAccess.GetTreatmentSymptomsList(languageCode);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success"));
                        return Json(new { Status = true, Data = result, user = User.Identity.Name});
                    }
                    else
                    {
                        _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.DataNotFound));
                        return Json(new { Status = true, Data = String.Empty, ErrorCode = Errors.DataNotFound.ErrorCode, ErrorMessage = Errors.DataNotFound.ErrorDescription, Error = Errors.DataNotFound });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("GetMemberInfo")]
        public IHttpActionResult GetMemberInfo(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string MemberNo = form.Get("MemberNo");
                    List<MemberInfo> result = GardaMedikaAccess.GetMemberInfo(MemberNo);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Succes"));
                        return Json(new { Status = true, Data = result, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                    else
                    {
                        return Json(new { Status = false, Data = String.Empty, Message = "Failed", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
                }
            }
        }

        [HttpPost, Route("GetReimbursementTreatmentType")]
        public IHttpActionResult GetReimbursementTreatmentType(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string memberNo = form.Get("MemberNo");
                    string treatmentStart = form.Get("TreatmentStart");
                    string treatmentFinish = form.Get("TreatmentFinish");
                    string langCode = form.Get("LangCode");
                    List<TreatmentType> result = GardaMedikaAccess.GetTreatmentType(memberNo, treatmentStart, treatmentFinish, langCode);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Succes"));
                        return Json(new { Status = true, Data = result, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                    else
                    {
                        return Json(new { Status = true, Data = String.Empty, Message = "Failed", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
                }
            }
        }

        [HttpPost, Route("GetReimbursementTreatmentDetailType")]
        public IHttpActionResult GetReimbursementTreatmentDetailType(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string memberNo = form.Get("MemberNo");
                    string treatmentStart = form.Get("TreatmentStart");
                    string treatmentFinish = form.Get("TreatmentFinish");
                    string treatmentType = form.Get("TreatmentType");
                    string langCode = form.Get("LangCode");

                    List<string> param = new List<string>();
                    param.Add(memberNo);
                    param.Add(treatmentStart);
                    param.Add(treatmentFinish);
                    param.Add(treatmentType);
                    param.Add(langCode);

                    List<TreatmentTypeDetail> result = GardaMedikaAccess.GetTreatmentTypeDetail(param);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Succes"));
                        return Json(new { Status = true, Data = result, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                    else
                    {
                        return Json(new { Status = true, Data = String.Empty, Message = "Failed", ErrorCode = Errors.DataNotFound.ErrorCode, ErrorMessage = Errors.DataNotFound.ErrorDescription, Error = Errors.DataNotFound });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
                }
            }
        }

        //[HttpPost, Route("ValidateReimbursementTreatment")]
        //public IHttpActionResult ValidateReimbursementTreatment(FormDataCollection form)
        //{
        //    _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
        //    if (form == null)
        //    {
        //        _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
        //        return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
        //    }
        //    else
        //    {
        //        try
        //        {
        //            string claimID = form.Get("ClaimID");
        //            ReimbursementData reimbursementData = GardaMedikaAccess.GetReimbursementData(claimID);

        //            List<ValidationResult> result = GardaMedikaAccess.ValidateReimbursementTreatment(reimbursementData);

        //            return Json(new { Status = true, ValidationResult = result, Message = "Success" });
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
        //            return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
        //        }
        //    }
        //}

        [HttpPost, Route("GetReimbursementPrepostData")]
        public IHttpActionResult GetReimbursementPrepostData(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string memberNo = form.Get("MemberNo");
                    string treatmentStart = form.Get("TreatmentStart"); //dd/MMM/yyyy !
                    string treatmentType = form.Get("TreatmentType");

                    List<string> param = new List<string>();
                    param.Add(memberNo);
                    param.Add(treatmentStart);
                    param.Add(treatmentType);
                    param.Add(String.Empty);

                    List<PrepostClaim> result = GardaMedikaAccess.ReimbursementPrepostData(param);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Succes"));
                        return Json(new { Status = true, Data = result, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                    else
                    {
                        return Json(new { Status = true, Data = String.Empty, Message = "Failed", ErrorCode = Errors.DataNotFound.ErrorCode, ErrorMessage = Errors.DataNotFound.ErrorDescription, Error = Errors.DataNotFound });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
                }
            }
        }

        [HttpPost, Route("ProcessReimbursementData")]
        public IHttpActionResult ProcessReimbursementData(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    ReimbursementData param = new ReimbursementData();
                    param = JsonConvert.DeserializeObject<ReimbursementData>(form.Get("Data"));

                    string userID = ConfigurationManager.AppSettings["UserIdSys"];
                    string langID = form.Get("LangID");

                    bool result = GardaMedikaAccess.ProcessReimbursementData(param, userID);

                    if (result)
                    {
                        List<ValidationResult> validationResult = GardaMedikaAccess.ValidateReimbursementTreatment(param, langID);

                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success"));
                        return Json(new { Status = true, Data = validationResult, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                    else
                    {
                        return Json(new { Status = false, Data = String.Empty, Message = "Failed", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
                }
            }
        }


        [HttpPost, Route("GetClaimReimbursementStatus")]
        public IHttpActionResult GetClaimReimbursementStatus(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string claimID = form.Get("ClaimID");

                    var reimbursementData = GardaMedikaAccess.GetReimbursementData(claimID);
                    try
                    {
                        if ((reimbursementData.Status ?? "").In(ReimbursementStatus.DOCUMENT_INCOMPLETE, ReimbursementStatus.CLAIM_REJECTED))
                        {
                            reimbursementData.CustomReason = JsonConvert.DeserializeObject<List<ReimbursementIncompleteReason>>(reimbursementData.Remarks ?? "");
                        }
                    }
                    catch (Exception e1)
                    {
                        //throw new Exception(e1.ToString());
                        _logger.Debug(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e1.ToString()));
                    }

                    if (reimbursementData != null)
                    {
                        return Json(new { Status = true, Data = reimbursementData, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                    else
                    {
                        return Json(new { Status = false, Data = String.Empty, Message = "Failed", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
                }
            }
        }

        //[HttpPost, Route("SubmitReimbursementData")]
        //public IHttpActionResult SubmitReimbursementData(FormDataCollection form)
        //{
        //    _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
        //    if (form == null)
        //    {
        //        _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
        //        return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
        //    }
        //    else
        //    {
        //        try
        //        {
        //            string claimID = form.Get("ClaimID");
        //            string claimNo = form.Get("ClaimNo");
        //            string relatedClaimNo = form.Get("RelatedClaimNo");
        //            string symptomData = form.Get("SymptomData");
        //            string userID = ConfigurationManager.AppSettings["UserIdSys"];

        //            List<string> param = new List<string>();
        //            param.Add(claimNo);
        //            param.Add(relatedClaimNo);
        //            param.Add(symptomData);
        //            param.Add(userID);
        //            param.Add(claimID);

        //            bool result = GardaMedikaAccess.SubmitReimbursementData(param);

        //            if (result)
        //            {
        //                _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Succes"));
        //                return Json(new { Status = true, Data = String.Empty, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
        //            }
        //            else
        //            {
        //                return Json(new { Status = false, Data = String.Empty, Message = "Failed", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
        //            return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
        //        }
        //    }
        //}

        [HttpPost, Route("CheckIsReimbursementNeedSymptoms")]
        public IHttpActionResult CheckIsReimbursementNeedSymptoms(FormDataCollection form)
        {
            try
            {
                string claimID = form.Get("ClaimID");
                bool result = GardaMedikaAccess.CheckIsReimbursementNeedSymptoms(claimID);

                _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Succes"));
                return Json(new { Status = true, Data = result, Message = true, ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });

            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
            }
        }

        [HttpPost, Route("GetReimbursementDetail")]
        public IHttpActionResult GetReimbursementDetail(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string claimID = form.Get("ClaimID");
                    string memberNo = form.Get("MemberNo");
                    string languageCode = form.Get("LangCode");
                    ReimbursementDetail result = GardaMedikaAccess.GetReimbursementDetail(claimID, memberNo, languageCode);

                    if (!String.IsNullOrEmpty(result.MemberNo))
                    {
                        result.Account = GardaMedikaAccess.GetAccountInfo(result.MemberNo);
                    }

                    if (!String.IsNullOrEmpty(result.RelatedClaimNo))
                    {
                        DateTime treatmentStart = DateTime.ParseExact(result.TreatmentStart, "dd/MM/yyyy", null);
                        List<string> Param = new List<string>();
                        Param.Add(result.MemberNo);
                        Param.Add(treatmentStart.ToString("dd/MMM/yyyy"));
                        Param.Add(string.Empty);
                        Param.Add(result.RelatedClaimNo);

                        List<PrepostClaim> prepostClaims = GardaMedikaAccess.ReimbursementPrepostData(Param);
                        if (prepostClaims.Count > 0)
                        {
                            result.RelatedClaim = prepostClaims[0];
                        }
                    }

                    _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success"));
                    return Json(new { Status = true, Data = result });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("UpdateReimbursementPrepost")]
        public IHttpActionResult UpdateReimbursementPrepost(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            try
            {
                string memberNo = form.Get("MemberNo");
                string claimID = form.Get("ClaimID");
                string relatedClaimNo = form.Get("RelatedClaimNo");
                string treatmentCode = form.Get("TreatmentCode");
                bool result = GardaMedikaAccess.UpdateReimbursementPrepost(relatedClaimNo, claimID, memberNo, treatmentCode);

                if (result)
                {
                    _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success"));
                    return Json(new { Status = true, Data = String.Empty, Message = "Success", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                }
                else
                {
                    return Json(new { Status = false, Data = String.Empty, Message = "Failed", ErrorCode = String.Empty, ErrorMessage = String.Empty, Error = String.Empty });
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, (ApplicationSettingAccess.isShowDetailMessage ? e.ToString() : String.Empty)), Data = String.Empty, Error = Errors.UnhandledException });
            }
        }

        [HttpPost, Route("GetReimbursementFileTypeList")]
        public IHttpActionResult GetReimbursementFileTypeList(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string langCode = form.Get("LangCode");
                    string claimID = form.Get("ClaimID");
                    string memberNo = form.Get("MemberNo");

                    List<ReimbursementFileType> result = GardaMedikaAccess.GetReimbursementFileTypeList(langCode, claimID, memberNo);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success"));
                        return Json(new { Status = true, Data = result });
                    }
                    else
                    {
                        _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.DataNotFound));
                        return Json(new { Status = true, Data = String.Empty, ErrorCode = Errors.DataNotFound.ErrorCode, ErrorMessage = Errors.DataNotFound.ErrorDescription, Error = Errors.DataNotFound });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("UploadImage")]
        public IHttpActionResult UploadImage(FormDataCollection form )
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string thumbnail = form.Get("Thumbnail");
                    byte[] thumbnailData = InternalFunction.StringToByte(thumbnail);

                    ReimbursementFileImage fileImage = new ReimbursementFileImage()
                    {
                        ClaimID = form.Get("ClaimID"),
                        DeviceID = form.Get("DeviceID"),
                        ImageData = thumbnailData,
                        ImageExt = form.Get("Extension"),
                        ImageID = System.Guid.NewGuid().ToString(),
                        PathFile = string.Empty,
                        ReimbursementImageID = string.Empty,
                        ThumbnailData = thumbnailData,
                        EntryUsr = ConfigurationManager.AppSettings["UserIdSys"],
                        IsMandatory = bool.Parse(form.Get("IsMandatory")),
                        ImageType = form.Get("ImageType")
                    };

                    string result = GardaMedikaAccess.UploadImage(fileImage);
                    UploadImageResponse response = null;
                    if (!string.IsNullOrEmpty(result))
                    {
                        response = new UploadImageResponse()
                        {
                            ImageID = result,
                            ThumbnailData = thumbnailData
                        };
                    }

                    _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success"));
                    return Json(new { Status = true, Data = response });

                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("GetReimbursementFileExample")]
        public IHttpActionResult GetReimbursementFileExample(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage("Start: " + DateTime.Now.ToString("dd/mm/yyyy hh:mm:ss")));


            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string languageCode = form.Get("LangCode");
                    string imageExampleCode = form.Get("ImageExampleCode");
                    List<ReimbursementImageExample> result = GardaMedikaAccess.GetReimbursementFileExample(imageExampleCode, languageCode);

                    if (result.Count > 0)
                    {
                        _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("Success" + DateTime.Now.ToString("dd/mm/yyyy hh:mm:ss")));
                        return Json(new { Status = true, Data = result });
                    }
                    else
                    {
                        _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.DataNotFound));
                        return Json(new { Status = true, Data = String.Empty, ErrorCode = Errors.DataNotFound.ErrorCode, ErrorMessage = Errors.DataNotFound.ErrorDescription, Error = Errors.DataNotFound });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("GetProviderInfoFullText")]
        public IHttpActionResult GetProviderInfoFullText(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _logger.Debug(actionName + " Function start: ");
            try
            {
                var SearchKeyword = form.Get("SearchKeyword");

                var result = SEADataAccess.GetSEAFullTextIndexData
                    (
                        SearchKeyword,
                        "PROVIDER"
                    );

                return Json(new { Status = true, Data = result, ErrorCode = "", ErrorMessage = "" });
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
            }
        }

        [HttpPost, Route("GetProviderInfoByDescription")]
        public IHttpActionResult GetProviderInfoByDescription(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _logger.Debug(actionName + " Function start: ");
            try
            {
                var result = SEADataAccess.GetProviderInfoByDescription();

                return Json(new { Status = true, Data = result, ErrorCode = "", ErrorMessage = "" });
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
            }
        }

#if DEBUG
        [HttpPost, Route("GenerateAuthenticationKey"), AllowAnonymous]
        public IHttpActionResult GenerateAuthenticationKey(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {
                string memberNo = form.Get("MemberNo");
                string deviceID = form.Get("DeviceID");
                string claimID = form.Get("ClaimID");
                string langID = (form.Get("LangID") ?? "").ToUpper();
                string module = (form.Get("Module") ?? "").ToUpper();

                MedcareObject medcareUserObject = GardaMobileCMSAccess.GetMedcareDataObject(memberNo, deviceID);

                if (String.IsNullOrWhiteSpace(memberNo) || String.IsNullOrWhiteSpace(deviceID) || !module.In("REIMBURSEMENT", "DOCUMENT-RESUBMIT", "CLAIM-INFO") || medcareUserObject == null ||
                    !langID.In("EN", "ID"))
                {
                    throw new Exception("Invalid Parameter");
                }
                else if
                    (
                        module.In("DOCUMENT-RESUBMIT") && String.IsNullOrWhiteSpace(claimID)
                    )
                {
                    throw new Exception("Invalid Parameter: No Claim ID");
                }
                else
                {

                    List<AuthenticationParameter> authParam = new List<AuthenticationParameter>();
                    authParam.Add(new AuthenticationParameter("ClaimID", claimID));
                    AuthenticationObject authObject = new AuthenticationObject();
                    
                    var cookies = Request.Headers.GetCookies("public-anonymous").FirstOrDefault();
                    var anonymousID = cookies["public-anonymous"].Value;
                    authObject.AnonymousID = anonymousID;
                    authObject.DeviceID = medcareUserObject.ClientDeviceID;
                    authObject.CustomerID = medcareUserObject.CustomerID;
                    authObject.LangID = langID;
                    authObject.Module = "REIMBURSEMENT";
                    authObject.ValidFrom = DateTime.Now.ToString();
                    authObject.ValidTo = DateTime.Now.AddHours(24).ToString();
                    authObject.MemberNo = medcareUserObject.MemberNo;
                    authObject.AuthorizationParameter = authParam;


                    if (module.ToUpper().Equals("REIMBURSEMENT"))
                    {
                        authParam.Add(new AuthenticationParameter("Path", "/reimbursement/member-form"));
                    }
                    else if (module.ToUpper().Equals("DOCUMENT-RESUBMIT"))
                    {
                        authParam.Add(new AuthenticationParameter("Path", "/reimbursement/document-resubmit-form"));
                    }
                    else if (module.ToUpper().Equals("CLAIM-INFO"))
                    {
                        authParam.Add(new AuthenticationParameter("Path", "/claim-info"));
                    }

                    //< Route exact path = "/reimbursement/member-form" />
                    //< Route exact path = "/reimburement/incomplete-document-info" />
                    //< Route exact path = "/reimburement/document-waiting-hardcopy-info" />
                    //< Route exact path = "/reimburement/claim-rejected-info" />      
                    //< Route exact path = "/reimburement/document-resubmit-form" />
                    //< Route exact path = "/reimburement/document-form" />
                    //< Route exact path = "/reimburement/document-example-form" />
                    //< Route exact path = "/reimburement/reimbursement-summary" />                                       

                    authObject.AuthKey = Guid.NewGuid().ToString();
                    string authKey = HttpUtility.UrlEncode(authObject.AuthKey).ToString();
                    string encryptedAuthKey = AppAESHelper.SignatureEncrypt(authObject.AuthKey);
                    authObject.EncryptedAuthKey = encryptedAuthKey;
                    string key = HttpUtility.UrlEncode(encryptedAuthKey);

                    GardaMobileCMSAccess.SaveMedcareAuthKey(authObject, claimID);

                    authObject = GardaMobileCMSAccess.GetAuthenticationObject(authObject.AuthKey);
                    authObject.AuthKey = authKey;
                    authObject.EncryptedAuthKey = encryptedAuthKey;

                    var authResponse = Authentication.AuthenticateMedcareSignature(ref authObject);

                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.DataNotFound));
                                        
                    return Json(new { Status = true, AuthKey = key, AuthObject = authObject, AuthResponse = authResponse, ErrorCode = "", ErrorMessage = "" });
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Data = e.ToString(), ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
            }

        }

        [HttpGet, Route("TestFetch"), AllowAnonymous]
        public IHttpActionResult TestFetch(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {

                List<AuthenticationParameter> authParam = new List<AuthenticationParameter>();
                authParam.Add(new AuthenticationParameter("Path", "/reimbursement/member-form"));
                authParam.Add(new AuthenticationParameter("ClaimID", ""));
                //< Route exact path = "/reimbursement/member-form" />
                //< Route exact path = "/reimburement/incomplete-document-info" />
                //< Route exact path = "/reimburement/document-waiting-hardcopy-info" />
                //< Route exact path = "/reimburement/claim-rejected-info" />      
                //< Route exact path = "/reimburement/document-resubmit-form" />
                //< Route exact path = "/reimburement/document-form" />
                //< Route exact path = "/reimburement/document-example-form" />
                //< Route exact path = "/reimburement/reimbursement-summary" />

                AuthenticationObject authObject = new AuthenticationObject();
                authObject.AnonymousID = "718e8beeb6f5abe06f16301806250c2c395af1f6de5cb7418c8777ef18a132ac";
                authObject.DeviceID = "AB1B5E0D-AD7B-47A5-B713-065E39810473";
                authObject.CustomerID = "AB1B5E0D-AD7B-47A5-B713-065E39810474";
                authObject.LangID = "EN";
                authObject.Module = "REIMBURSEMENT";
                authObject.ValidFrom = DateTime.Now.ToString();
                authObject.ValidTo = DateTime.Now.AddHours(24).ToString();
                authObject.MemberNo = "H/00047166";
                authObject.AuthorizationParameter = authParam;

                string authKey = HttpUtility.UrlEncode(AppAESHelper.SignatureEncrypt(JsonConvert.SerializeObject(authObject))).ToString();

                string key = HttpUtility.UrlEncode("OVLdrhCbZ/0n7RL7X9+GgjsPtRyQABOIWPkp9dr4qZrVORpTS2O/xXUdekHrGsRO");

                //AppAESHelper.SignatureEncrypt(Guid.NewGuid().ToString());

                AuthenticationObject decryptResult = JsonConvert.DeserializeObject<AuthenticationObject>(AppAESHelper.SignatureDecrypt(HttpUtility.UrlDecode(authKey)));


                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.DataNotFound));

                return Json(new { Status = true, AuthKey = authKey, key, Object = decryptResult, ErrorCode = "", ErrorMessage = "" });
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Data = e.ToString(), ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
            }

        }
#endif

        [HttpPost, Route("DeleteImage")]
        public IHttpActionResult DeleteImage(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string reimbursementImageID = form.Get("ReimbursementImageID");
                    string userID = ConfigurationManager.AppSettings["UserIdSys"];

                    bool result = GardaMedikaAccess.DeleteImage(reimbursementImageID, userID);
                    return Json(new { Status = true, Data = result });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("SubmitReimbursement")]
        public IHttpActionResult SubmitReimbursement(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string claimID = form.Get("ClaimID");
                    string memberNo = form.Get("memberNo");
                    string symptomList = form.Get("SymptomList");

                    string userID = ConfigurationManager.AppSettings["UserIdSys"];

                    bool result = GardaMedikaAccess.SubmitReimbursement(claimID, memberNo, userID, symptomList, ReimbursementStatus.PROCESS_INPUT, ReimbursementStatus.CLAIM_SUBMITTED);
                    return Json(new { Status = true, Data = result });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("ResubmitReimbursement")]
        public IHttpActionResult ResubmitReimbursement(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string claimID = form.Get("ClaimID");
                    string memberNo = form.Get("memberNo");
                    string userID = ConfigurationManager.AppSettings["UserIdSys"];

                    bool result = GardaMedikaAccess.ResubmitReimbursement(claimID, memberNo, userID, ReimbursementStatus.DOCUMENT_INCOMPLETE, ReimbursementStatus.CLAIM_DOCUMENT_RESUBMITTED);
                    return Json(new { Status = true, Data = result });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("CheckNeedHardCopy")]
        public IHttpActionResult CheckNeedHardCopy(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string memberNo = form.Get("MemberNo");
                    int amountBilled = int.Parse(form.Get("AmountBilled"));
                    //List<string> reimbursementStatus = new List<string>();
                    //reimbursementStatus.Add(ReimbursementStatus.WAITING_FOR_HARDCOPY);
                    //reimbursementStatus.Add(ReimbursementStatus.DOCUMENT_INCOMPLETE);

                    bool result = GardaMedikaAccess.CheckNeedHardCopy(memberNo, amountBilled);
                    return Json(new { Status = true, Data = result });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("DeleteTempImage")]
        public IHttpActionResult DeleteTempImage(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string imageID = form.Get("ImageID");
                    string claimID = form.Get("ClaimID");

                    bool result = GardaMedikaAccess.DeleteTempImage(imageID, claimID);
                    return Json(new { Status = true, Data = result });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }

        [HttpPost, Route("DownloadFile")]
        public IHttpActionResult DownloadFile(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            if (form == null)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.EmptyParameter));
                return Json(new { Status = false, Message = Errors.EmptyParameter.ErrorDescription, Data = String.Empty, Error = Errors.EmptyParameter });
            }
            else
            {
                try
                {
                    string fileType = form.Get("FileType");
                    string filePath = string.Empty;

                    switch (fileType.ToUpper())
                    {
                        case "FORMREIMBURSEMENT":
                            filePath = ConfigurationManager.AppSettings["FormReimbursementClaimPath"];
                            break;
                        default:
                            break;
                    }

                    byte[] fileBytes = File.ReadAllBytes(filePath);
                    string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);

                    return Json(new { Status = true, Data = new { FileBytes = fileBytes, FileName = fileName } });
                }
                catch (Exception e)
                {
                    _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                    return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
                }
            }
        }
    }
}