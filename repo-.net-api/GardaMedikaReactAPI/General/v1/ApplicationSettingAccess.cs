﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Configuration;

namespace GardaMedikaReactAPI.General.v1
{
    internal static class ApplicationSettingAccess
    {

        internal static string GetAppConfig(string configName)
        {
            return WebConfigurationManager.AppSettings[configName] ?? null;
        }

        #region ApplicationID
        internal const string APPLICATIONID_ASURANSIASTRAWEB = "ASURANSIASTRA";
        internal const string APPLICATIONID_GARDAOTOWEB = "GARDAOTO";
        internal const string APPLICATIONID_OTOCARE = "OTOCARE";
        #endregion

        #region General Settings
        internal static string ApplicationID = GetAppConfig("ApplicationID");
        internal static string ApplicationCode = GetAppConfig("ApplicationCode");
        internal static string ApplicationName = GetAppConfig("ApplicationName");
        internal static string ModuleName = GetAppConfig("ModuleName");
        internal static string SmsAPIUrl = GetAppConfig("SmsAPIUrl");
        internal static string SmtpHost = GetAppConfig("SmtpHost");
        internal static string SmtpPort = GetAppConfig("SmtpPort");
        internal static string DomainControllerAddress = GetAppConfig("DomainControllerAddress");
        internal static string UserActiveDirectory = GetAppConfig("UserActiveDirectory");
        internal static string PasswordActiveDirectory = GetAppConfig("PasswordActiveDirectory");
        internal static string JWTValidHours = GetAppConfig("JWTValidHours");
        internal static string TransactionalDataLogging = GetAppConfig("TransactionalDataLogging");
        internal static string GLOBAL_APPLICATION_CODE = GetAppConfig("GlobalApplicationCode");
        internal static string GLOBAL_SERVICE_ADDRESS = GetAppConfig("GlobalServiceAddress");
        internal static string JWT_SIGNING_KEY = GetAppConfig("JWTSigningKey");
        internal static string JWT_VALID_HOURS = GetAppConfig("JWTValidHours");
        internal static bool ALLOW_UNTRUSTED_SSL = Int32.Parse(GetAppConfig("allowUntrustedSSL") ?? "0") == 1 ? true : false;
        internal static bool ALLOW_USER_PASSWORD_RESET = Int32.Parse(GetAppConfig("AllowedUserPasswordReset") ?? "0") == 1 ? true : false;
        internal static bool RECORD_METHOD_EXECUTION_TIME = Int32.Parse(GetAppConfig("RecordMethodExecutionTime") ?? "0") == 1 ? true : false;
        internal static string BaseAPIURL = GetAppConfig("BaseAPIUrl");
        internal static bool isShowDetailMessage = Int32.Parse(GetAppConfig("isShowDetailMessage") ?? "0") == 1 ? true : false;
        #endregion

    }

}
