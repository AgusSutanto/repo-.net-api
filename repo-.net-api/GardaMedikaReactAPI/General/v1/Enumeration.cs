﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GardaMedikaReactAPI.General.v1
{
    internal class ReimbursementStatus
    {
        public const string CLAIM_APPROVED = "A";
        public const string HARDCOPY_DOCUMENT_RECEIVED ="C";
        public const string CLAIM_DOCUMENT_RESUBMITTED = "D";
        public const string WAITING_FOR_HARDCOPY = "H";
        public const string DOCUMENT_INCOMPLETE = "I";
        public const string PROCESS_INPUT = "N";
        public const string CLAIM_PAID = "P";
        public const string CLAIM_REJECTED ="R";
        public const string CLAIM_SUBMITTED = "S";
    }

    internal class Modules
    {
        public const string REIMBURSEMENT = "REIMBURSEMENT";
        public const string REUPLOAD_REIMBURSEMENT = "REUPLOAD_REIMBURSEMENT";
    }

}