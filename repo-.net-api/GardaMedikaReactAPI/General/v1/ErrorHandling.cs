﻿/*
 *  Error Handling Class
 *  Dev : HIT
 *  Date: 23/11/2017
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using a2is.Framework.Monitoring;
using System.Text;
using System.Web.Helpers;


namespace GardaMedikaReactAPI.General.v1
{
    internal static class ErrorHandling
    {
        internal static string GetFunctionBeginMessage(params string[] AdditionalMessage)
        {
            return new StringBuilder().AppendFormat(
                "{0} Function Start: {1} {2}.",
                ApplicationSettingAccess.isShowDetailMessage ? "" : new StackTrace().GetFrame(1).GetMethod().Name,
                DateTime.Now,
                InternalFunction.IsNullOrEmpty(AdditionalMessage) ? "" : String.Concat("Additional Info: ", System.Web.Helpers.Json.Encode(AdditionalMessage))
            ).ToString();
        }

        internal static string GetFunctionErrorMessage(Error e, params string[] AdditionalMessage)
        {
            return new StringBuilder().AppendFormat(
                "{0} Function Error: {1} {2} {3}.",
                ApplicationSettingAccess.isShowDetailMessage ? "" : new StackTrace().GetFrame(1).GetMethod().Name,
                DateTime.Now,
                System.Web.Helpers.Json.Encode(e),
                InternalFunction.IsNullOrEmpty(AdditionalMessage) ? "" : String.Concat("Additional Info: ", System.Web.Helpers.Json.Encode(AdditionalMessage))
            ).ToString();
        }

        internal static string GetFunctionDebugMessage(params string[] AdditionalMessage)
        {
            return new StringBuilder().AppendFormat(
                "{0} Function Debug: {1} {2}.",
                ApplicationSettingAccess.isShowDetailMessage ? "" : new StackTrace().GetFrame(1).GetMethod().Name,
                DateTime.Now,
                InternalFunction.IsNullOrEmpty(AdditionalMessage) ? "" : String.Concat("Additional Info: ", System.Web.Helpers.Json.Encode(AdditionalMessage))
            ).ToString();
        }

        internal static string GetFunctionSuccessMessage(params string[] AdditionalMessage)
        {
            return new StringBuilder().AppendFormat(
                "{0} Function OK: {1} {2}.",
                ApplicationSettingAccess.isShowDetailMessage ? "" : new StackTrace().GetFrame(1).GetMethod().Name,
                DateTime.Now,
                InternalFunction.IsNullOrEmpty(AdditionalMessage) ? "" : String.Concat("Additional Info: ", System.Web.Helpers.Json.Encode(AdditionalMessage))
            ).ToString();
        }
    }

    public class Error
    {
        private static int staticID = 0;

        public Error() { }
        public Error(string ErrorDescription)
        {

            this.ErrorDescription = ErrorDescription;

            this.ErrorCode = String.Format("{0}{1}", "E", String.Concat("00000000000000000000000000", staticID.ToString()).Right(5));
            staticID += 1;

        }

        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public string ErrorDataObject { get; set; }
    }



    public static class Errors
    {

        #region API Action Errors
        public static Error UnhandledException = new Error("Unhandled Exception Occured."); //0
        public static Error InvalidFileExtension = new Error("Invalid File Extension."); //1
        public static Error MaximumFilenameLengthExceed = new Error("Filename is too long.");//2
        public static Error UploadedFileContainsNoData = new Error("Uploaded file contains no data.");//3
        public static Error InvalidParameter = new Error("Invalid Parameter.");//4
        public static Error EmptyParameter = new Error("Empty Parameter.");//5
        public static Error InvalidProcess = new Error("Invalid Process.");//6
        public static Error InvalidPaymentMethod = new Error("Invalid Payment Method.");//7
        public static Error RenewalOrderExceedPeriodError = new Error("Renewal Order Date Exceed Policy Period To.");//8
        public static Error InvalidDataError = new Error("Invalid Data.");//9
        public static Error RenewalOrderProcessPendingError = new Error("Renewal Order is exists and still proceessing.");//10
        public static Error RenewalOrderProcessRejectedError = new Error("Renewal Order is already rejected.");//11
        public static Error RenewalOrderProcessAlreadyApprovedError = new Error("Renewal Order is already approved.");//12
        public static Error PolicyExistsNotRenewableError = new Error("Policy Data exists but not renewable");//13
        public static Error PlateNotValid = new Error("Plate Not Valid");        //14
        public static Error PolicyEligibleForRenewal = new Error("Vehicle is eligible for renewal");//15
        public static Error RenewableVehicleNotRegistered = new Error("Renewable vehicle hasn't registered in your device.");//16
        public static Error RenewableVehicleIsNotSupportedSegment = new Error("Renewable vehicle segment is not supported.");//17    
        public static Error RenewableVehicleViaGODigital = new Error("Renewable vehicle via GODigital");//18
        public static Error DataNotFound = new Error("Data not found");//19

        #endregion

        #region Authorization Errors

        public static Error InvalidSignatureKey = new Error("InvalidSignatureKey"); //20

        #endregion

    }

}
