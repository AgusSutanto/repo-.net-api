﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace GardaMedikaReactAPI.General.v1
{
    public class LogFormat
    {
        public static string BeginMsgInfo(string fn, string param)
        {
            return string.Format("Begin {0} - param : {1}", fn, param);
        }

        public static string BeginMsgInfo(string fn, object param)
        {
            return string.Format("Begin {0} - param : {1}", fn, JsonConvert.SerializeObject(param));
        }

        public static string SuccessMsgInfo(string fn)
        {
            return string.Format("Success {0}", fn);
        }

        public static string EndMsgInfo(string fn)
        {
            return string.Format("End {0}", fn);
        }

        public static string ErrorMsgInfo(string fn, string param)
        {
            return string.Format("Error {0} - param : {1} - Throwing exception", fn, param);
        }

        public static string ErrorMsgInfo(string fn, string param, string msg)
        {
            return string.Format("Error {0} - {2} - param : {1} - Throwing exception", fn, param, msg);
        }
    }
}