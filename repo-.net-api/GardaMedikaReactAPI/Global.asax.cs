﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.Web.Http;
using a2is.Framework.API;
using a2is.Framework.Monitoring;
using a2is.Framework.Security;
using GardaMedikaReactAPI.App_Start;

namespace GardaMedikaReactAPI
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            a2isAPIConfig.Configure();
            //AreaRegistration.RegisterAllAreas();
            WebAPIConfig.Register(GlobalConfiguration.Configuration);
        }
    }
}