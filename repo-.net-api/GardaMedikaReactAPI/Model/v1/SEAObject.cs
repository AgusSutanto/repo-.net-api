﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GardaMedikaReactAPI.Model.v1
{
    public class SEAObject
    {
        public class MemberInfo
        {
            public MemberInfo() { }
            public string MemberName { get; set; }
            public string MemberNo { get; set; }
            public string EmployeeID { get; set; } //NPK
            public string ClientID { get; set; } //Perusahaan
            public int PNO { get; set; }
            public int MNo { get; set; }
            public DateTime? PDate { get; set; }
            public DateTime? PPDate { get; set; }
        }

        public class Symptom
        {
            public Symptom() { }
            public string SymptomCode { get; set; }
            public string SymptomDescription { get; set; }
        }

        public class ReimbursementDetail
        {
            public ReimbursementDetail() { }
            public string MemberNo { get; set; }
            public string Name { get; set; }
            public string Nominal { get; set; }
            public string TreatmentStart { get; set; }
            public string TreatmentDescription { get; set; }
            public string RelatedClaimNo { get; set; }
            public AccountInfo Account { get; set; }
            public PrepostClaim RelatedClaim { get; set; }
            public string ProviderName { get; set; }
        }

        public class AccountInfo
        {
            public AccountInfo() { }
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public string BankBranch { get; set; }
            public string AccountNo { get; set; }
            public string AccountName { get; set; }
            public string Nationality { get; set; }
            public string AccountType { get; set; }
        }

        public class PrepostClaim
        {
            public PrepostClaim() { }
            public string Provider { get; set; }
            public string TreatmentStart { get; set; }
            public string TreatmentEnd { get; set; }
            public decimal Billed { get; set; }
            public string ClaimNo { get; set; }
            public string TreatmentCode { get; set; }
        }

        public class TreatmentType
        {
            public TreatmentType() { }
            public string TreatmentCode { get; set; }
            public string TreatmentDescription { get; set; }
        }

        public class TreatmentTypeDetail
        {
            public TreatmentTypeDetail() { }
            public string TreatmentDetailDescription { get; set; }
            public string TreatmentDetailCode { get; set; }
        }

        public class ReimbursementFileType
        {
            public ReimbursementFileType() { }
            public string DocumentTypeID { get; set; }
            public string DocumentTypeName { get; set; }
            public bool IsMandatory { get; set; }
            public List<ReimbursementFileImage> Images { get; set; }
        }

        public class ReimbursementDataList
        {
            public List<ReimbursementData> Reimbursement { get; set; } = new List<ReimbursementData>();
        }

        public class ReimbursementData
        {
            public ReimbursementData() { }
            public string ClaimID { get; set; }
            public string MemberNo { get; set; }
            public string MemberNoLogin { get; set; }
            public string TreatmentStart { get; set; }
            public string TreatmentEnd { get; set; }
            public string TreatmentPlace { get; set; }
            public string TreatmentPlaceCode { get; set; }
            public decimal AmountBilled { get; set; }
            public decimal MaximumAmountBilled { get; set; }
            public string TreatmentCode { get; set; }
            public string TreatmentDetailCode { get; set; }
            public string ClaimNo { get; set; }
            public string RelatedClaimNo { get; set; }
            public string SymptomClaimData { get; set; }
            public string DeviceID { get; set; }
            public string Status { get; set; }
            public string Remarks { get; set; }
            public List<ReimbursementIncompleteReason> CustomReason { get; set; }
            public List<ReimbursementFileImage> File { get; set; }
        }


        public class ReimbursementFileImage
        {
            public ReimbursementFileImage() { }
            public string ReimbursementImageID { get; set; }
            public string ClaimID { get; set; }
            public string ImageID { get; set; }
            public string DeviceID { get; set; }
            public string ImageType { get; set; }
            public string ImageExt { get; set; }
            public string PathFile { get; set; }
            public byte[] ImageData { get; set; }
            public byte[] ThumbnailData { get; set; }
            public bool IsMandatory { get; set; }
            public string EntryUsr { get; set; }
            //public DateTime EntryDt { get; set; }
            //public string UpdateUsr { get; set; }
            //public DateTime UpdateDt { get; set; }
        }

        public class ValidationResult
        {
            public ValidationResult() { }
            public string ValidationCode { get; set; }
            public string ValidationValue { get; set; }
        }

        public class ReimbursementImageExample
        {
            public ReimbursementImageExample() { }
            public string ImageExampleCode { get; set; }
            public string ImageExampleCaption { get; set; }
            public string ImageExt { get; set; }
            public byte[] ImageData { get; set; }
            public byte[] ThumbnailData { get; set; }
            public string PathFile { get; set; }
        }


        public class SEAFullTextIndex
        {
            public SEAFullTextIndex() { }
            public Int64 ID { get; set; }
            public string DataID { get; set; }
            public string DataTerm { get; set; }
            public string DataDescription { get; set; }
            public string DataType { get; set; }
            public string DataModule { get; set; }
            public int RowStatus { get; set; }
        }

        public class UploadImageResponse
        {
            public UploadImageResponse() { }
            public string ImageID { get; set; }
            public byte[] ThumbnailData { get; set; }
        }

        public class ProviderInfoByDescription
        {
            public string ID { get; set; }
            public string Description { get; set; }
        }

        public class ReimbursementIncompleteReason
        {
            public string DocumentType { get; set; }
            public List<ReimbursementReasonMessage> Reason { get; set; }
        }

        public class ReimbursementReasonMessage
        {
            public string Code { get; set; }
            [JsonExtensionData]
            public Dictionary<string, object> ExtensionData { get; set; }
        }
    }
}