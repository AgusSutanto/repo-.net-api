﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using a2is.Framework.DataAccess;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Model.v1;

namespace GardaMedikaReactAPI.Repository
{
    internal class ApplicationDBParameter : RepositoryBase
    {
       
        internal static string GetAppParamValueGen5Health(string paramName)
        {
            using (var db = GetA2isHealthDB())
            {
                ApplicationOption result = db.Fetch<ApplicationOption>(QueryCollection.qGetGen5HealthApplicationParamValue, paramName).FirstOrDefault();
                return result == null ? "" : result.OptionValue;
            }
        }

        internal static List<LanguageStaticText> GetStaticLanguageContent(string langID)
        {
            using (var db = GetA2isHealthDB())
            {
                List<LanguageStaticText> result = db.Fetch<LanguageStaticText>(QueryCollection.qGetLanguageStaticText, langID);
                return result;
            }
        }
    }
}