﻿using a2is.Framework.Monitoring;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Model.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GardaMedikaReactAPI.Repository.v1
{
    internal class  GardaMobileCMSAccess
    {
        #region Logger
        private static readonly a2isLogHelper _logger = new a2isLogHelper();
        #endregion

        internal static AuthenticationObject GetAuthenticationObject(string authKey)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());            
            AuthenticationObject result = null;

            try
            {
                using (var db = RepositoryBase.GetA2isGardaMobileCMSDB())
                {
                    string authObject  = db.Fetch<string>
                        (
                            QueryCollection.QueryGetGardaMobileCMSAuthenticationData,
                            authKey
                        ).FirstOrDefault();

                    result = JsonConvert.DeserializeObject<AuthenticationObject>(authObject);
                }

                _logger.Debug(ErrorHandling.GetFunctionSuccessMessage());
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return null;
            }

            return result;
        }

        internal static MedcareObject GetMedcareDataObject(string memberNo, string deviceID)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {
                using (var db = RepositoryBase.GetA2isGardaMobileCMSDB())
                {
                   return db.Fetch<MedcareObject>
                        (
                            QueryCollection.QueryGetMedcareDataObject,
                            memberNo,
                            deviceID
                        ).FirstOrDefault();
                    
                }              
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return null;
            }
        }

        internal static int UpdateMedcareLogClaimID(AuthenticationObject authObject, AuthenticationResponse authResponse)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {
                using (var db = RepositoryBase.GetA2isGardaMobileCMSDB())
                {
                   return db.Execute
                         (
                             QueryCollection.QueryUpdateMedcareAuthkeyClaimID,
                             authObject.AuthKey,
                             authResponse.ClaimID                            
                         );

                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return -1;
            }
        }

        internal static int SaveMedcareAuthKey(AuthenticationObject authObject, string claimID = null)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {
                using (var db = RepositoryBase.GetA2isGardaMobileCMSDB())
                {
                    return db.Execute
                         (
                             QueryCollection.QuerySaveMedcareAuthData,
                             authObject.AuthKey,
                             authObject.EncryptedAuthKey,
                             JsonConvert.SerializeObject(authObject),
                             authObject.MemberNo,
                             authObject.DeviceID,
                             authObject.CustomerID,
                             claimID
                         );

                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return -1;
            }
        }
        
    }
}