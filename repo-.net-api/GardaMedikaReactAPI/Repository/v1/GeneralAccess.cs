﻿using a2is.Framework.Monitoring;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Model.v1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static GardaMedikaReactAPI.Model.v1.SEAObject;

namespace GardaMedikaReactAPI.Repository.v1
{
    internal class GeneralAccess
    {
        #region Logger
        private static readonly a2isLogHelper _logger = new a2isLogHelper();
        #endregion

        internal static bool SavingAuthenticationHistory(AuthenticationObject authObject =  null, string exceptionMessage = "")
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    //Remove Language Static Text on Logging
                    db.Execute
                        (
                            QueryCollection.QuerySaveAuthenticationHistory,
                            authObject == null? "" : authObject.AuthKey,
                            authObject == null ? "" : authObject.EncryptedAuthKey,
                            authObject == null? null : JsonConvert.SerializeObject(authObject),
                            authObject == null? "" : authObject.MemberNo,
                            authObject == null? "" : authObject.CustomerID,
                            authObject?.AuthorizationParameter?.Find(x=>x.ParameterName == "ClaimID")?.ParameterValue,
                            String.IsNullOrWhiteSpace(exceptionMessage)? 1 : 0,
                            exceptionMessage
                        );
                }

                _logger.Debug(ErrorHandling.GetFunctionSuccessMessage());

                return true;
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return false;
            }
        }
    }
}