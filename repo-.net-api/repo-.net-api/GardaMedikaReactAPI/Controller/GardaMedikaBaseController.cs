﻿/**********************************************************
 * DEV: HIT
 * Description: Controller Masking 
 **********************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using a2is.Framework.Filters;
using a2is.Framework.Monitoring;
using a2is.Framework.Filters;
using a2is.Framework.Monitoring;
using System.Web.Http;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using log4net;
using System.Threading;
using System.Reflection;
using System.Net.Http;
using System.Net;
using GardaMedikaReactAPI.Repository.v1;

namespace GardaMedikaReactAPI
{
    [a2isLoggerFilter]
    [Authorize]
    public class GardaMedikaBaseController : ApiController
    {
        internal static ReactMedcareAESHelper AppAESHelper = ReactMedcareAESHelper.Instances;
        internal static a2isLogHelper _logger = new a2isLogHelper();

        public static object ChatbotAESHelper { get; private set; }

        #region validation
        protected bool isValidRequiredFields(params string[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(values[i]))
                    return false;
            }
            return true;
        }

        #endregion

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);
            MethodBase method = sf.GetMethod();

            return string.Format("{0}.{1}", method.DeclaringType, method.Name);
        }
    }

   
}