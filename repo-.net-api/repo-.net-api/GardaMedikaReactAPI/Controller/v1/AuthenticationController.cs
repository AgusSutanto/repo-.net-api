﻿/**********************************************************
 * DEV: HIT
 * Description: a2is Retail Xoom Authentication Controller
 **********************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Configuration;
using System.Globalization;
using a2is.Framework.API;
using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using a2is.Framework.Security;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Principal;
using System.Data;
using System.IdentityModel.Tokens;
using System.IO;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Specialized;
using System.Dynamic;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using GardaMedikaReactAPI.Controller;
using GardaMedikaReactAPI.Repository;
using GardaMedikaReactAPI.General;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Model.v1;
using GardaMedikaReactAPI.Repository.v1;

namespace GardaMedikaReactAPI.Controller.v1
{
    public class AuthenticationController : GardaMedikaBaseController
    {
        [HttpPost, Route("AuthenticateSignature"), AllowAnonymous]
        public IHttpActionResult AuthenticateSignature(FormDataCollection form)
        {
            string fn = GetCurrentMethod();
            _logger.Debug(LogFormat.BeginMsgInfo(fn, "Start :"));

            string param = form.Get("authkey");
            AuthenticationObject authObject = new AuthenticationObject(param, param); ;
            AuthenticationResponse authResponse = null;

            try
            {
                if (form == null || String.IsNullOrWhiteSpace(param))
                {
                    _logger.Error(LogFormat.ErrorMsgInfo(fn, Errors.EmptyParameter.ErrorDescription));

                    throw new Exception(String.Format("Invalid parameter : {0}", "Form / authkey is null"));
                }
                else
                {

                     var cookies = Request.Headers.GetCookies("public-anonymous").FirstOrDefault();
                     
                     var anonymousID = cookies["public-anonymous"].Value;
          
                    
                    try
                    {
                        string authKey = AppAESHelper.SignatureDecrypt(param);  
                        
                        authObject = GardaMobileCMSAccess.GetAuthenticationObject(authKey);
                                                
                        _logger.Debug(ErrorHandling.GetFunctionDebugMessage(String.Format("Headers Cookies:{0}", JsonConvert.SerializeObject(Request.Headers.GetCookies()))));

                        authObject.AuthKey = authKey;
                        authObject.EncryptedAuthKey = param;
                        authObject.AnonymousID = anonymousID ?? String.Empty;

                        if (anonymousID == null)
                        {
                            throw new Exception(String.Format("Invalid Authentication Key : {0}", "Unmatched anonymousID"));
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("Failed to decode Authentication Key : {0}", e.ToString()));
                    }

                    authResponse = Authentication.AuthenticateMedcareSignature(ref authObject);
                    
                    if (!authResponse.Status || authResponse.MemberInfo == null)
                    {
                        throw new Exception(String.Format("Invalid Authentication Key : {0}", "Failed to authenticate Medcare Signature"));
                    }
                    else
                    {
                        //Authentication Request/Response History Logging.
                        new Task(() =>
                        {
                            GeneralAccess.SavingAuthenticationHistory(authObject, String.Empty);
                        }).Start();
                    }

                    _logger.Debug(ErrorHandling.GetFunctionSuccessMessage("OK!"));
                    return Json(authResponse);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));

                //Authentication Request/Response History Logging.
                new Task(() =>
                {
                    GeneralAccess.SavingAuthenticationHistory(authObject,e.ToString());
                }).Start();

                #if DEBUG 
                return Json( new { Status = false, Message = ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()) , Data = "", Error = Errors.UnhandledException});
                #else
                return Json(new { Status = false, Message = String.Empty, Data = "", Error = Errors.UnhandledException });
                #endif
            }
        }

        [HttpPost, Route("CheckTokenValidity")]
        public IHttpActionResult CheckTokenValidity(FormDataCollection form)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            try
            {
                ErrorHandling.GetFunctionDebugMessage("Function OK!");
                return Json(new { Status = true, Data = "", ErrorCode = "", ErrorMessage = "", Error = "" });
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return Json(new { Status = false, Data = "", ErrorCode = Errors.UnhandledException.ErrorCode, ErrorMessage = Errors.UnhandledException.ErrorDescription, Error = e.ToString() });
            }
        }
    }
}