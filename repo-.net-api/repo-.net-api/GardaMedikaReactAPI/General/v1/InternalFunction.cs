﻿using System;
using System.Web.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http.Formatting;
using System.Configuration;
using System.Globalization;
using a2is.Framework.API;
using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using static a2is.Framework.DataAccess.a2isDBHelper;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using a2is.Framework.Security;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Dynamic;
using System.Reflection;


namespace GardaMedikaReactAPI.General.v1
{
    internal static class InternalFunction 
    {
        internal static string GenerateMailContent(string Template, List<string> Value)
        {
            for (int i = 0; i < Value.Count; i++)
            {
                Template = Template.Replace(string.Format("[{0}{1}]", "@@", i), !string.IsNullOrWhiteSpace(Value[i]) ? Value[i] : "");
            }
            return Template;
        }

        internal static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        internal static string RemoveWhiteSpaces(string s)
        {
            return string.Join(" ", s.Split(new char[] { ' ' },
                   StringSplitOptions.RemoveEmptyEntries));
        }

        internal static bool IsNullOrEmpty(this Array array)
        {
            return (array == null || array.Length == 0);
        }

        internal static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        internal static string ReverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
                       
        internal static bool IsDate(string date)
        {
            DateTime temp;
            if (DateTime.TryParse(date, out temp))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        internal static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        internal static string md5Hash(int hash)
        {
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(hash.ToString());
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        }

        internal static dynamic ReconstructObject<T>(T ConstructedObject, List<string> SelectedObjectProperty = null)
        {
            dynamic result = new ExpandoObject();
           
            if (ConstructedObject != null && SelectedObjectProperty != null)
            {
                foreach (PropertyInfo prop in ConstructedObject.GetType().GetProperties())
                {
                    if (SelectedObjectProperty.Contains(prop.Name))
                        AddProperty(result, prop.Name, prop.GetValue(ConstructedObject));
                }
            }          

            return result;
        }

        internal static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

        internal static byte[] StringToByte(string plainText)
        {
            byte[] byteData = new byte[plainText.Length / 2];
            for (int i = 0; i < byteData.Length; i++)
            {
                string a = plainText.Substring(i * 2, 2);
                byte b = Convert.ToByte(plainText.Substring(i * 2, 2), 16);
                byteData[i] = Convert.ToByte(plainText.Substring(i * 2, 2), 16);

            }
            return byteData;

        }

    }




    internal static class IEnumerableExtension
    {
        public static IEnumerable<T> Safe<T>(this IEnumerable<T> source)
        {
            if (source == null)
            {
                yield break;
            }

            foreach (var item in source)
            {
                yield return item;
            }
        }
    }
}