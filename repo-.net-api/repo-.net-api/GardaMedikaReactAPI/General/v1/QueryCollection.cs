﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GardaMedikaReactAPI.General.v1
{
    internal static class QueryCollection
    {
        #region 'Param Value'

        #region 'Medcare Application Param Value'
        internal static string qGetMedcareApplicationParamValue = @"";
        #endregion

        #region 'Gen5 Health Application Param Value'
        internal static string qGetGen5HealthApplicationParamValue = @"select 'REIMBURSEMENT' as ApplicationName, [key] as OptionName,value as OptionValue from Config_Parameters where [key] = @0";

        internal static string qGetLanguageStaticText = 
@";SELECT  LangTextCode,LangID,LangTextDescription
FROM    Medcare.Lang_Text
WHERE   LangModule = 'LGN002'
        AND LangID = @0
";
        #endregion

        #region 'Get GardaMobileCMS Authentication Data'
        internal static string QueryGetGardaMobileCMSAuthenticationData =
@"SELECT TOP 1 AuthObject
FROM    dbo.MedcareAuthKeyLog
WHERE   AuthKey = @0";
        #endregion

        internal static string QueryGetMedcareDataObject =
@"SELECT  ma.MemberNo ,
        d.DeviceID ,
        d.ClientDeviceID ,
        am.CustomerID
FROM    medcare.dbo.MemberActivation ma
        INNER JOIN GardaMobileCMS.dbo.AccountMobile am ON ma.AccountMobileID = am.AccountMobileID
        INNER JOIN GardaMobileCMS.dbo.Device d ON d.DeviceToken = am.CustomerID
WHERE   MemberNo = @0
        AND ClientDeviceID = @1
        AND am.ApplicationID = 1 ";

        internal static string QueryUpdateMedcareAuthkeyClaimID =
@";UPDATE  dbo.MedcareAuthKeyLog
SET     ClaimID = @1
WHERE   AuthKey = @0
AND ISNULL(Authkey,'')<> '';
";

        internal static string QuerySaveMedcareAuthData =
@"INSERT INTO GardaMobileCMS.dbo.MedcareAuthKeyLog
        ( AuthKey ,
		  EncryptedAuthKey,
		  AuthObject,
          MemberNo ,
          DeviceID ,
          CustomerID ,
		  ClaimID,
          EntryDt
        )
SELECT @0, --AuthKey ,
	   @1,--EncryptedAuthKey,
	   @2,--AuthObject,
	   @3,--MemberNo ,
	   @4,--DeviceID ,
	   @5,--CustomerID ,
	   @6,--ClaimID,
	   GETDATE()--EntryDt";
        #endregion

        #region Authentication History
        internal static string QuerySaveAuthenticationHistory =
@";IF NOT EXISTS ( SELECT  1
                FROM    Medcare.AuthenticationHistory
                WHERE   EncryptedAuthKey = @1 )
    AND (@1 IS NOT NULL)
    OR (1 = 1)
    BEGIN
        INSERT  INTO medcare.AuthenticationHistory
                ( AuthKey ,
                  EncryptedAuthKey,
                  AuthObject ,
                  MemberNo ,
                  CustomerID,
                  ClaimID,
                  IsValidF ,
                  ExceptionMessage,
                  EntryDt 
                )
        VALUES  ( @0 , -- AuthKey - varchar(100)
                  @1 , -- EncryptedAuthKey -- varchar(100)
                  @2 , -- AuthObject - varchar(max)                  
                  @3 , -- MemberNo - varchar(50)
                  @4 , -- CustomerID - varchar(100)
                  @5 , -- ClaimID - varchar(11)
                  @6 , -- IsValidF - smallint
                  @7 , -- ExceptionMessage
                  GETDATE()  -- EntryDt - datetime         
                )
    END
ELSE
    BEGIN
        UPDATE  medcare.AuthenticationHistory
        SET     UpdatedDt = GETDATE()
        WHERE   EncryptedAuthKey = @1
    END
 
";

        #endregion  

        #region 'Get Reimbursement'

        #region 'Get Reimbursement Data'
        internal static string qGetReimbursementData =
@"SELECT  ClaimID ,
        MemberNo ,
        TreatmentStart ,
        TreatmentEnd ,
        TreatmentCode ,
        TreatmentDetailCode ,
        ClaimNo ,
        RelatedClaimNo ,
        SymptomClaimData ,
        Nominal AmountBilled,
        MaximumAmountBilled,
        DeviceID,
        Remarks,
        Status
FROM    Medcare.Reimbursement_Transaction
WHERE ClaimID = @0 ";
        #endregion

        #region Get Reimbursement File Image
        internal static string qGetReimbursementFileImage = @"SELECT  ReimbursementImageID ,
                                                                    ClaimID ,
                                                                    ImageID ,
                                                                    DeviceID ,
                                                                    RTRIM(ImageType) AS ImageType ,
                                                                    ImageExt ,
                                                                    PathFile ,
                                                                    ThumbnailData
                                                            FROM    Medcare.Reimbursement_Image
                                                            WHERE   ClaimID = @0  AND RowStatus = 0";
        #endregion

        #endregion

        #region 'Get Provider Info By Description'
        internal static string qGetProviderInfoByDesc =
@"SELECT DataID as ID, DataDescription as Description FROM dbo.SEAFullTextIndex 
WHERE DataModule = 'MEDCARE'
AND DataType = 'PROVIDER' 
/*DataDescription*/
ORDER BY [Description] ASC";
        #endregion

        #region 'Symtoms List'
        internal static string qSymtomList =
@"SELECT  s.SymptomCode AS ID ,
                ISNULL(l.LangTextDescription, s.SymptomDescription) AS Description
        FROM    Medcare.Symptom s
                LEFT JOIN Medcare.Lang_Text l ON s.LangTextCode = l.LangTextCode
                                                 AND l.LangModule = 'LGN001'
                                                 AND l.LangID = @0";
        #endregion

        #region 'Process Reimbursement Data'
        internal static string qProcessReimbursementData=
@";DECLARE @@ClaimID CHAR(11) = @0
DECLARE @@MemberNo VARCHAR(20) = @1
DECLARE @@TreatmentStart DATETIME = @2
DECLARE @@TreatmentEnd DATETIME = @3
DECLARE @@TreatmentCode CHAR(6) = @4
DECLARE @@TreatmentDetailCode CHAR(6)= @5
DECLARE @@ProviderID VARCHAR(10)= @6
DECLARE @@ProviderName VARCHAR(1000) = @7
DECLARE @@AmountBilled FLOAT = @8
DECLARE @@DeviceID VARCHAR(200) = @9
DECLARE @@Status CHAR(1) = @10
DECLARE @@EntryUsr CHAR(5) = @11
DECLARE @@MemberNoLogin varchar(20) = @12
DECLARE @@AmountBilledMax FLOAT = ( SELECT  value AmountBilledMax
                                    FROM    dbo.Config_Parameters
                                    WHERE   [key] = 'MAX-AMOUNT-BILLED'
                                            AND category = 'REIMBURSEMENT'
                                  )

IF NOT EXISTS ( SELECT  1
                FROM    Medcare.Reimbursement_Transaction
                WHERE   ClaimID = @@ClaimID )
    BEGIN
        INSERT  INTO Medcare.Reimbursement_Transaction
                ( ClaimID ,
                  MemberNo ,
                  MemberNoLogin,
                  TreatmentStart ,
                  TreatmentEnd ,
                  TreatmentCode ,
                  TreatmentDetailCode ,
                  ProviderID ,
                  ProviderName ,
                  Nominal ,
                  MaximumAmountBilled,
                  DeviceID ,
                  Status ,
                  RowStatus ,
                  EntryUsr ,
                  EntryDt
                )
        VALUES  ( @@ClaimID , -- ClaimID - char(10)
                  @@MemberNo , -- MemberNo - varchar(20)
                  @@MemberNoLogin, -- MemberNoLogin -- varchar(20)
                  @@TreatmentStart , -- TreatmentStart - datetime
                  @@TreatmentEnd , -- TreatmentEnd - datetime
                  @@TreatmentCode , -- TreatmentCode - char(6)
                  @@TreatmentDetailCode , -- TreatmentDetailCode - char(6)
                  @@ProviderID , -- ProviderID
                  @@ProviderName , -- ProviderName
                  @@AmountBilled , -- Nominal,
                  @@AmountBilledMax, -- MaximumAmountBilled
                  @@DeviceID , -- DeviceID - varchar(200)
                  @@Status , -- Status - smallint
                  0 , -- RowStatus - smallint
                  @@EntryUsr , -- EntryUsr - char(5)
                  GETDATE()
                );
    END;
ELSE
    BEGIN

		
        IF EXISTS ( SELECT  1
                    FROM    Medcare.Reimbursement_Transaction
                    WHERE   ClaimID = @@ClaimID
                            AND Status <> @@Status )
            BEGIN

                INSERT  INTO Medcare.Hst_Reimbursement_Transaction
                        ( ClaimID ,
                          Status_Before ,
                          Status_After ,
                          EntryUsr ,
                          EntryDt 
	                    )
                        SELECT  ClaimID ,
                                Status,
                                @@Status ,
                                @@EntryUsr ,
                                GETDATE()
                        FROM    Medcare.Reimbursement_Transaction
                        WHERE   ClaimID = @@ClaimID;
            END;

        DECLARE @@OldTreatmentCode CHAR(6),
                    @@OldTreatmentDetailCode CHAR(6);

        SELECT @@OldTreatmentCode = TreatmentCode,
                @@OldTreatmentDetailCode = TreatmentDetailCode
        FROM Medcare.Reimbursement_Transaction
        WHERE ClaimID = @@ClaimID;

        IF @@OldTreatmentCode <> @@TreatmentCode
            OR @@OldTreatmentDetailCode <> @@TreatmentDetailCode
        BEGIN
            UPDATE Medcare.Temp_Reimbursement_Image
            SET RowStatus = 1
            WHERE ClaimID = @@ClaimID;
        END;

        UPDATE  Medcare.Reimbursement_Transaction
        SET     MemberNo = @@MemberNo ,
                TreatmentStart = @@TreatmentStart ,
                TreatmentEnd = @@TreatmentEnd ,
                TreatmentCode = @@TreatmentCode ,
                TreatmentDetailCode = @@TreatmentDetailCode ,
                ProviderID = @@ProviderID ,
                ProviderName = @@ProviderName ,
                Nominal = @@AmountBilled ,
				Status = @@Status,
                UpdateUsr = @@EntryUsr ,
                UpdateDt = GETDATE()
        WHERE   ClaimID = @@ClaimID;
    END ";
        #endregion

        #region 'Submit Reimbursement Data'

        #region 'Insert Reimbursement Image'
        internal static string qInsertReimbursementImage =
@";INSERT  INTO Medcare.Reimbursement_Image
        ( ReimbursementImageID ,
          ClaimID ,
          ImageID ,
          DeviceID ,
          ImageType ,
          PathFile ,
          RowStatus ,
          ImageData ,
          ThumbnailData ,
          IsMandatory,
          EntryUsr ,
          EntryDt
        )
        SELECT  NEWID(),
				ClaimID,
				ImageID,
				DeviceID,
				ImageType,
				PathFile,
				RowStatus,
				ImageData,
				ThumbnailData,
                IsMandatory,
				EntryUsr,
				EntryDt
        FROM    Medcare.Temp_Reimbursement_Image
        WHERE   ClaimID = @0 ";
        #endregion

        #region 'Update Reimbursement Transaction'
        internal static string qUpdateReimbursementTransaction =
@";UPDATE  Medcare.Reimbursement_Transaction
SET     ClaimNo = @0 ,
        RelatedClaimNo = @1 ,
        SymptomClaimData = @2,
		UpdateUsr = @3,
		UpdateDt = GETDATE()
WHERE   ClaimID = @4 ";
        #endregion

        #endregion

        #region 'Get Reimbursement Prepost Data' 
        internal static string qGetReimbursementPrepostData =
@";DECLARE @@MemberNo VARCHAR(20) = @0;
DECLARE @@TreatmentStart DATE = @1;
DECLARE @@ClaimNo VARCHAR(50) = @3

DECLARE @@PreDay INT = 0;
DECLARE @@PreLimit INT = 0;
DECLARE @@PostDay INT = 0;
DECLARE @@PostLimit INT = 0;
DECLARE @@PrePostLimit INT = 0;
DECLARE @@ProductType VARCHAR(25) = @2;


 

DECLARE @@OPNo INT = 0;
DECLARE @@MNo INT = 0;

SELECT TOP 1
        @@MNo = PM.MNo ,
        @@OPNo = P.OPNO ,
        @@ProductType = PR.ProductType
FROM    Policy P
        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
        INNER JOIN Policy_Member_Plan PMP ON PM.PNO = PMP.PNo
                                             AND PM.MNo = PMP.MNo
                                             AND PMP.ExcludeF = 0
        INNER JOIN PPlan PP ON PMP.PPlan = PP.PPlan
        INNER JOIN Product PR ON PP.ProductID = PR.ProductID
WHERE   PM.MemberNo = @@MemberNo
        AND PR.ProductType IN ( 'IP', 'IP-MA' )
        AND @@TreatmentStart BETWEEN PM.PDate AND PM.PPDate
        AND PM.TType <> 'R'
ORDER BY PM.MNo DESC;

SELECT  B.BenefitID ,
        B.IsPre ,
        B.IsPost ,
        PBS.PreDay ,
        PBS.PreLimit ,
        PBS.PostDay ,
        PBS.PostLimit ,
        PBS.PrePostLimit
INTO    #BenefitPrePost
FROM    Policy P
        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
        INNER JOIN Policy_Member_Plan PMP ON PM.PNO = PMP.PNo
                                             AND PM.MNo = PMP.MNo
                                             AND PMP.ExcludeF = 0
        INNER JOIN Policy_Plan_Schedule PPS ON PMP.PNo = PPS.PNO
                                               AND PMP.PPlan = PPS.PPlan
                                               AND PMP.BAmount = PPS.BAmount
        INNER JOIN Policy_Benefit_Schedule PBS ON PPS.PNO = PBS.PNO
                                                  AND PPS.PPlan = PBS.PPlan
                                                  AND PPS.BAmount = PBS.BAmount
        INNER JOIN Benefit B ON PBS.BenefitID = B.BenefitID
        INNER JOIN PPlan PP ON PBS.PPlan = PP.PPlan
        INNER JOIN Product PR ON PP.ProductID = PR.ProductID
WHERE   PM.MNo = @@MNo
        AND PR.ProductType = @@ProductType
        AND ( B.IsPre = 1
              OR B.IsPost = 1
            );

SELECT  @@PreDay = CASE WHEN COALESCE(PreDay, 0) = 0 THEN 999999999
                        ELSE PreDay
                   END ,
        @@PreLimit = CASE WHEN COALESCE(PreLimit, 0) = 0 THEN 999999999
                          ELSE PreLimit
                     END
FROM    #BenefitPrePost
WHERE   IsPre = 1;

SELECT  @@PostDay = CASE WHEN COALESCE(PostDay, 0) = 0 THEN 999999999
                         ELSE PostDay
                    END ,
        @@PostLimit = CASE WHEN COALESCE(PostLimit, 0) = 0 THEN 999999999
                           ELSE PostLimit
                      END
FROM    #BenefitPrePost
WHERE   IsPost = 1;

IF EXISTS ( SELECT TOP 1
                    1
            FROM    #BenefitPrePost
            WHERE   IsPre = 1
                    AND IsPost = 1 )
    BEGIN
        SELECT  @@PrePostLimit = CASE WHEN COALESCE(PrePostLimit, 0) = 0
                                      THEN 999999999
                                      ELSE PrePostLimit
                                 END
        FROM    #BenefitPrePost
        WHERE   IsPre = 1
                AND IsPost = 1;
    END;
ELSE
    BEGIN
        SET @@PrePostLimit = 999999999;
    END;

--SELECT @@ProductType AS ProductType,
--        @@PreDay AS PreDays,
--        @@PreLimit AS PreLimit,
--        @@PostDay AS PostDays,
--        @@PostLimit AS PostLimit,
--        @@PrePostLimit AS PrePostLimit,
--    @@OPNo AS OPNo

DROP TABLE #BenefitPrePost;

SELECT  *
INTO    #Claim
FROM    ( SELECT    COALESCE(CAST(TDRD.DocumentNo AS VARCHAR(MAX)), '') AS DocumentNo ,
                    CH.ClaimNo AS ClaimNo ,
                    CASE WHEN CH.Status = 'R' THEN 'Approved'
                         WHEN CH.Status = 'V' THEN 'Verified'
                         WHEN CH.Status = 'H' THEN 'Hold'
                         WHEN CH.Status = 'P' THEN 'Postponed'
                         WHEN CH.Status = 'O' THEN 'Open'
                         ELSE ''
                    END AS Status ,
                    PM.Name AS MemberName ,
                    COALESCE(CH.TreatmentPlace, TDRD.TreatmentPlace) AS Provider ,
                    COALESCE(D.Name, '') AS Diagnosis ,
                    CONVERT(VARCHAR(MAX), FORMAT(CH.Start, 'dd/MM/yyyy')) AS TreatmentStart ,
                    CONVERT(VARCHAR(MAX), FORMAT(CH.Finish, 'dd/MM/yyyy')) AS TreatmentEnd ,
                    COALESCE(CH.Billed, 0) AS Billed ,
                    COALESCE(CH.Accepted, 0) AS Accepted ,
                    CH.ProductType AS ProductType ,
                    CH.RoomOption AS RoomOption ,
                    CH.CNO AS CNo ,
                    0 AS NumOfPreClaim ,
                    0 AS NumOfPostClaim ,
                    @@PreLimit AS PreLimit ,
                    @@PostLimit AS PostLimit ,
                    @@PrePostLimit AS PrePostLimit ,
                    @@PreDay AS PreDay ,
                    @@PostDay AS PostDay
          FROM      ClaimH CH
                    INNER JOIN Policy_Member PM ON CH.MNO = PM.MNo
                    INNER JOIN Policy P ON PM.PNO = P.PNO
                                           AND P.OPNO = @@OPNo
                    LEFT JOIN tbl_disp_registration_document TDRD ON CH.ClaimNo = TDRD.ClaimNo
                    LEFT JOIN Claim_Diagnosis CDG ON CH.CNO = CDG.CNO
                                                     AND CDG.IsPrimary = 1
                    LEFT JOIN Diagnosis D ON CDG.DiagnosisID = D.Diagnosis
          WHERE     PM.MemberNo = @@MemberNo
                    AND CH.ProductType = @@ProductType
                    AND CH.Status NOT IN ( 'C', 'X' )
                    AND DATEDIFF(DAY, CH.Start, CH.Finish) >= 1

                                --AND CH.ClaimNo <> @@ClaimNo
                    AND ( DATEDIFF(DAY, CH.Finish, @@TreatmentStart) BETWEEN 0
                                                              AND
                                                              @@PostDay
                          OR DATEDIFF(DAY, @@TreatmentStart, CH.Start) BETWEEN 0
                                                              AND
                                                              @@PreDay
                        )
          UNION ALL
          SELECT    CAST(TDRD.DocumentNo AS VARCHAR(MAX)) AS DocumentNo ,
                    '' AS ClaimNo ,
                    CASE WHEN TDRD.DocumentStatus = 'SCOR' THEN 'Follow Up'
                         WHEN TDRD.Status = 'REGISTERED'
                         THEN 'Document Registered'
                         WHEN TDRD.Status = 'SEND' THEN 'Document Sent'
                         WHEN TDRD.Status = 'CHECKED' THEN 'Document Checked'
                         WHEN TDRD.Status = 'REJECT' THEN 'Rejected'
                         ELSE ''
                    END AS Status ,
                    PM.Name AS MemberName ,
                    TDRD.TreatmentPlace AS Provider ,
                    D.Name AS Diagnosis ,
                    CONVERT(VARCHAR(MAX), FORMAT(TDRD.TreatmentStart,
                                                 'dd/MM/yyyy')) AS TreatmentStart ,
                    CONVERT(VARCHAR(MAX), FORMAT(TDRD.TreatmentEnd,
                                                 'dd/MM/yyyy')) AS TreatmentEnd ,
                    COALESCE(TDRD.TotalBill, 0) AS Billed ,
                    0 AS Accepted ,
                    TDRD.ProductType AS ProductType ,
                    TDRD.RoomOption AS RoomOption ,
                    0 AS CNo ,
                    0 AS NumOfPreClaim ,
                    0 AS NumOfPostClaim ,
                    @@PreLimit AS PreLimit ,
                    @@PostLimit AS PostLimit ,
                    @@PrePostLimit AS PrePostLimit ,
                    @@PreDay AS PreDay ,
                    @@PostDay AS PostDay
          FROM      tbl_disp_registration_document TDRD
                    INNER JOIN Policy_Member PM ON TDRD.MNO = PM.MNo
                    INNER JOIN Policy P ON PM.PNO = P.PNO
                                           AND P.OPNO = @@OPNo
                    INNER JOIN Diagnosis AS D ON TDRD.DiagnosisID = D.Diagnosis
                    LEFT JOIN ClaimH CH ON TDRD.ClaimNo = CH.ClaimNo
          WHERE     CH.ClaimNo IS NULL
                    AND PM.MemberNo = @@MemberNo
                    AND TDRD.ProductType = @@ProductType
                    AND DATEDIFF(DAY, TDRD.TreatmentStart, TDRD.TreatmentEnd) >= 1

                                --AND TDRD.DocumentNo <> @@DocumentNo
                    AND ( DATEDIFF(DAY, TDRD.TreatmentEnd, @@TreatmentStart) BETWEEN 0
                                                              AND
                                                              @@PostDay
                          OR DATEDIFF(DAY, @@TreatmentStart,
                                      TDRD.TreatmentStart) BETWEEN 0
                                                           AND
                                                              @@PreDay
                        )
        ) X;
WITH    CTE
          AS ( SELECT   C.CNo ,
                        C.DocumentNo ,
                        CASE WHEN CONVERT(VARCHAR(MAX), FORMAT(CH.Start,
                                                              'dd/MM/yyyy')) <= C.TreatmentStart
                             THEN 1
                             ELSE 0
                        END AS IsPre ,
                        CASE WHEN CONVERT(VARCHAR(MAX), FORMAT(CH.Start,
                                                              'dd/MM/yyyy')) >= C.TreatmentEnd
                             THEN 1
                             ELSE 0
                        END AS IsPost
               FROM     #Claim C
                        INNER JOIN ClaimH CH ON CH.ReferenceClaimNo = C.ClaimNo
                                                AND CH.ClaimNo <> C.ClaimNo

                                --AND CH.ClaimNo <> @@ClaimNo
               WHERE    CH.PrePostStatus = 1
                        AND CH.Status NOT IN ( 'C', 'X' )
             ),
        CTE2
          AS ( SELECT   CNo ,
                        DocumentNo ,
                        SUM(IsPre) AS NumOfPreClaim ,
                        SUM(IsPost) AS NumOfPostClaim
               FROM     CTE
               GROUP BY CNo ,
                        DocumentNo
             )
    UPDATE  C
    SET     NumOfPreClaim = C.NumOfPreClaim + CTE2.NumOfPreClaim ,
            NumOfPostClaim = C.NumOfPostClaim + CTE2.NumOfPostClaim
    FROM    #Claim C
            INNER JOIN CTE2 ON C.CNo = CTE2.CNo
                               AND C.DocumentNo = CTE2.DocumentNo;
WITH    CTE
          AS ( SELECT   C.CNo ,
                        C.DocumentNo ,
                        CASE WHEN CONVERT(VARCHAR(MAX), FORMAT(CH.Start,
                                                              'dd/MM/yyyy')) <= C.TreatmentStart
                             THEN 1
                             ELSE 0
                        END AS IsPre ,
                        CASE WHEN CONVERT(VARCHAR(MAX), FORMAT(CH.Start,
                                                              'dd/MM/yyyy')) >= C.TreatmentEnd
                             THEN 1
                             ELSE 0
                        END AS IsPost
               FROM     #Claim C
                        INNER JOIN ClaimH CH ON CH.ReferenceDocumentNo = C.DocumentNo
                                                AND COALESCE(CH.ReferenceClaimNo,
                                                             '') = ''
                                                AND C.DocumentNo <> 0
                                --AND CH.ClaimNo <> @@ClaimNo
               WHERE    CH.PrePostStatus = 1
                        AND CH.Status NOT IN ( 'C', 'X' )
             ),
        CTE2
          AS ( SELECT   CNo ,
                        DocumentNo ,
                        SUM(IsPre) AS NumOfPreClaim ,
                        SUM(IsPost) AS NumOfPostClaim
               FROM     CTE
               GROUP BY CNo ,
                        DocumentNo
             )
    UPDATE  C
    SET     NumOfPreClaim = C.NumOfPreClaim + CTE2.NumOfPreClaim ,
            NumOfPostClaim = C.NumOfPostClaim + CTE2.NumOfPostClaim
    FROM    #Claim C
            INNER JOIN CTE2 ON C.CNo = CTE2.CNo
                               AND C.DocumentNo = CTE2.DocumentNo;

SELECT  Provider ,
        TreatmentStart ,
        TreatmentEnd ,
        Billed,
		ClaimNo,
        ProductType AS TreatmentCode
FROM    #Claim
WHERE ClaimNo = CASE WHEN @@ClaimNo <> '' THEN @@ClaimNo ELSE ClaimNo END
ORDER BY CAST(DocumentNo AS INT) DESC;

DROP TABLE #Claim
";
        #endregion

        #region 'Update Reimbursement Prepost Data'
        internal static string qUpdateReimbursementPrepost =
@";DECLARE @@RelatedClaimNo AS VARCHAR(20) = @0
DECLARE @@ClaimID AS VARCHAR(11) = @1
DECLARE @@MemberNo AS VARCHAR(20) = @2
DECLARE @@TreatmentCode AS VARCHAR(25) = @3

IF @@RelatedClaimNo <> ''
    BEGIN
        IF @@TreatmentCode = 'IP'
            BEGIN
                SET @@TreatmentCode = 'POSTIP'
            END

        IF @@TreatmentCode = 'MA'
            BEGIN
                SET @@TreatmentCode = 'POSTMA'
            END

        UPDATE  Medcare.Reimbursement_Transaction
        SET     RelatedClaimNo = @@RelatedClaimNo ,
                TreatmentCode = @@TreatmentCode
        WHERE   ClaimID = @@ClaimID
                AND MemberNo = @@MemberNo
                AND Status = 'N'
    END
ELSE
    BEGIN 
        UPDATE  Medcare.Reimbursement_Transaction
        SET     RelatedClaimNo = @@RelatedClaimNo
        WHERE   ClaimID = @@ClaimID
                AND MemberNo = @@MemberNo
                AND Status = 'N'
    END ";
        #endregion

        #region 'Check Is Reimbursement Need Symptoms'
        internal static string qCheckIsReimbursementNeedSymptoms = @"IF EXISTS
                                                                (
                                                                    SELECT 1
                                                                    FROM Medcare.Reimbursement_Transaction rt
                                                                    WHERE rt.ClaimID = @0
                                                                          AND rt.TreatmentCode = 'OP'
                                                                )
                                                                BEGIN
                                                                    IF EXISTS
                                                                    (
                                                                        SELECT 1
                                                                        FROM Medcare.Reimbursement_Transaction rt
                                                                            INNER JOIN Medcare.Temp_Reimbursement_Image tri
                                                                                ON tri.ClaimID = rt.ClaimID
                                                                        WHERE rt.ClaimID = @0
                                                                              AND rt.TreatmentCode = 'OP'
                                                                              AND tri.ImageType = 'INFDX'
                                                                              AND tri.RowStatus = 0
                                                                    )
                                                                        SELECT 0;
                                                                    ELSE
                                                                        SELECT 1;
                                                                END;
                                                                ELSE
                                                                    SELECT 0;";
        #endregion

        #region 'Member Info'
        internal static string qGetMemberInfo =
@";DECLARE @@MemberNo VARCHAR(20) = @0; --B/00022827

IF OBJECT_ID('tempdb..#TempMemberInfo') IS NOT NULL
    DROP TABLE	#TempMemberInfo;

CREATE TABLE #TempMemberInfo
    (
      RowNo BIGINT ,
      MemberName VARCHAR(50) ,
      EmployeeID VARCHAR(20) ,
      ClientID VARCHAR(10) ,
      MemberNo VARCHAR(10) ,
      PDate SMALLDATETIME ,
      PPDate SMALLDATETIME ,
      Membership VARCHAR(10)
    );

;WITH    cte
          AS ( SELECT   PM.EmpID ,
                        P.ClientID
               FROM     Policy P
                        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
               WHERE    PM.MemberNo = @@MemberNo
                        AND GETDATE() BETWEEN PDate AND PPDate
               GROUP BY PM.EmpID ,
                        P.ClientID
             )
    INSERT  INTO #TempMemberInfo
            ( RowNo ,
              MemberName ,
              EmployeeID ,
              ClientID ,
              MemberNo ,
              PDate ,
              PPDate ,
              Membership
            )
            SELECT  aa.RowNo ,
                    aa.MemberName ,
                    aa.EmployeeID ,
                    aa.ClientID ,
                    aa.MemberNo ,
                    aa.PDate ,
                    aa.PPDate ,
                    aa.Membership
            FROM    ( SELECT    ROW_NUMBER() OVER ( PARTITION BY PM.MemberNo ORDER BY MIN(pm.pdate) DESC ) AS RowNo ,
                                PM.Name AS MemberName ,
                                PM.EmpID AS EmployeeID , --NPK
                                P.ClientID , --Perusahaan
                                PM.MemberNo ,
                                MIN(PM.PDate) PDate ,
                                MAX(PM.PPDate) PPDate ,
                                PM.Membership
                      FROM      cte
                                INNER JOIN Policy P ON P.ClientID = cte.ClientID
                                INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
                                                              AND cte.EmpID = PM.EmpID
                      WHERE     GETDATE() BETWEEN PM.PDate AND PM.PPDate
                      GROUP BY  PM.Name ,
                                PM.EmpID ,
                                P.ClientID ,
                                PM.MemberNo ,
                                PM.Membership
                    ) aa
            WHERE   RowNo = 1
            ORDER BY aa.Membership ASC;

IF EXISTS ( SELECT  1
            FROM    #TempMemberInfo
            WHERE   MemberNo = @@MemberNo
                    AND Membership = '2. SPO' )
    BEGIN
        DELETE  FROM #TempMemberInfo WHERE Membership = '1. EMP'
    END

IF EXISTS ( SELECT  1
            FROM    #TempMemberInfo
            WHERE   MemberNo = @@MemberNo
                    AND Membership = '3. CHI' )
    BEGIN
        DELETE  FROM #TempMemberInfo WHERE Membership IN ( '1. EMP', '2. SPO' )
    END

SELECT  RowNo ,
        MemberName ,
        EmployeeID ,
        ClientID ,
        MemberNo ,
        PDate ,
        PPDate ,
        Membership
FROM    #TempMemberInfo

DROP TABLE #TempMemberInfo ";
        #endregion

        #region 'Treatment Type'

        #region 'Treatment Type'
        
        internal static string qGetTreatmentType =
@";DECLARE @@MemberNo VARCHAR(20)
DECLARE @@TreatmentStart DATE
DECLARE @@TreatmentFinish DATE
DECLARE @@LangCode VARCHAR(5)


SET @@MemberNo = @0
SET @@TreatmentStart = @1
SET @@TreatmentFinish = @2
SET @@LangCode = @3

IF OBJECT_ID('tempdb..#TempTreatmentType') IS NOT NULL
    DROP TABLE	#TempTreatmentType;

CREATE TABLE #TempTreatmentType
    (
      TreatmentCode VARCHAR(12),
	  TreatmentDescription VARCHAR(MAX)
    );

INSERT INTO #TempTreatmentType
	        ( TreatmentCode ,
	          TreatmentDescription
	        )
SELECT DISTINCT
        mt.TreatmentCode ,
		ISNULL(lt.LangTextDescription, mt.TreatmentDescription) AS TreatmentDescription
FROM    Policy P
        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
        INNER JOIN Policy_Member_Plan PMP ON PMP.PNo = P.PNO
                                             AND PMP.MNo = PM.MNo
                                             AND PMP.ExcludeF = 0
        INNER JOIN Policy_Benefit_Schedule PBS ON PBS.PNO = P.PNO
                                                  AND PBS.PPlan = PMP.PPlan
                                                  AND PBS.BAmount = PMP.BAmount
        INNER JOIN Benefit B ON PBS.BenefitID = B.BenefitID
        INNER JOIN Medcare.Mapping_Benefit_Treatment mbt ON mbt.BenefitID = B.BenefitID
        INNER JOIN Medcare.Mst_Treatment mt ON mbt.TreatmentCode = mt.TreatmentCode
        LEFT JOIN Medcare.Lang_Text lt ON lt.LangTextCode = mt.LangTextCode
                                          AND lt.LangModule = 'LGN001'
                                          AND lt.LangID = @@LangCode
WHERE   PM.MemberNo = @@MemberNo
        AND @@TreatmentStart BETWEEN PM.PDate AND PM.PPDate

IF @@TreatmentStart = @@TreatmentFinish
    BEGIN
        DELETE  FROM #TempTreatmentType
        WHERE   TreatmentCode IN('IP','MA','HCP','DTWT')
    END

IF @@TreatmentStart < @@TreatmentFinish
    BEGIN
        DELETE  FROM #TempTreatmentType
        WHERE   TreatmentCode IN('DT','OP','IM','GL','POSTIP','POSTMA','ODS','DISP')
    END

SELECT * FROM #TempTreatmentType 

DROP TABLE #TempTreatmentType";
        #endregion

        #region 'Treatment Type Detail'
        internal static string qGetTreatmentTypeDetail =
@";DECLARE @@MemberNo VARCHAR(20);
DECLARE @@TreatmentStart DATE;
DECLARE @@TreatmentFinish DATE;
DECLARE @@TreatmentCode VARCHAR(25);
DECLARE @@LangCode VARCHAR(5);
DECLARE @@TreatmentDetail AS TABLE
    (
      TreatmentDetail VARCHAR(MAX),
	  TreatmentDetailCode VARCHAR(20)
    );

DECLARE @@TempGlasses AS TABLE
    (
      IsGlassesFrame BIT ,
      IsGlassesLens BIT
    );
DECLARE @@TempSex AS TABLE ( Sex VARCHAR(1) );
 
SET @@MemberNo = @0
SET @@TreatmentStart = @1
SET @@TreatmentFinish = @2
SET @@TreatmentCode = @3
SET @@LangCode = @4

-----------------------------------------------------

IF @@TreatmentCode = 'IM'
    BEGIN

        INSERT  INTO @@TreatmentDetail
                ( TreatmentDetail,
				  TreatmentDetailCode
                )
                SELECT  ISNULL(lt.LangTextDescription,
                               md.TreatmentDetailDescription) AS TreatmentDescription,
						md.TreatmentDetailCode
                FROM    Medcare.Treatment_Detail md
                        LEFT JOIN Medcare.Lang_Text lt ON lt.LangTextCode = md.LangTextCode
                                                          AND lt.LangModule = 'LGN001'
                                                          AND lt.LangID = @@LangCode
                WHERE   Status = 1
                        AND TreatmentCode = @@TreatmentCode
    END;

IF @@TreatmentCode = 'DT'
    BEGIN

        INSERT  INTO @@TreatmentDetail
                ( TreatmentDetail,
				  TreatmentDetailCode
                )
                SELECT  ISNULL(lt.LangTextDescription,
                               md.TreatmentDetailDescription) AS TreatmentDescription,
							   md.TreatmentDetailCode
                FROM    Medcare.Treatment_Detail md
                        LEFT JOIN Medcare.Lang_Text lt ON lt.LangTextCode = md.LangTextCode
                                                          AND lt.LangModule = 'LGN001'
                                                          AND lt.LangID = @@LangCode
                WHERE   Status = 1
                        AND TreatmentCode = @@TreatmentCode;

    END;

IF @@TreatmentCode = 'GL'
    BEGIN
        INSERT  INTO @@TreatmentDetail
                ( 
			      TreatmentDetail,
				  TreatmentDetailCode
                )
                SELECT  
                        ISNULL(lt.LangTextDescription,
                               md.TreatmentDetailDescription) AS TreatmentDescription, 
                        md.TreatmentDetailCode
                FROM    Medcare.Treatment_Detail md
                        LEFT JOIN Medcare.Lang_Text lt ON lt.LangTextCode = md.LangTextCode
                                                          AND lt.LangModule = 'LGN001'
                                                          AND lt.LangID = @@LangCode
                WHERE   Status = 1
                        AND TreatmentCode = @@TreatmentCode;

        INSERT  INTO @@TempGlasses
                ( IsGlassesFrame ,
                  IsGlassesLens
                )
                SELECT  DISTINCT
                        B.IsGlassesFrame ,
                        B.IsGlassesLens
                FROM    Policy P
                        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
                        INNER JOIN Policy_Member_Plan PMP ON PMP.PNo = P.PNO
                                                             AND PMP.MNo = PM.MNo
                                                             AND PMP.ExcludeF = 0
                        INNER JOIN Policy_Benefit_Schedule PBS ON PBS.PNO = P.PNO
                                                              AND PBS.PPlan = PMP.PPlan
                                                              AND PBS.BAmount = PMP.BAmount
                        INNER JOIN Benefit B ON PBS.BenefitID = B.BenefitID
                WHERE   PM.MemberNo = @@MemberNo
                        AND @@TreatmentStart BETWEEN PM.PDate AND PM.PPDate
                        AND ( B.IsGlassesFrame <> 0
                              OR B.IsGlassesLens <> 0
                            );

        --IF NOT EXISTS ( SELECT  1
        --                FROM    @@TempGlasses
        --                WHERE   IsGlassesFrame = 1
        --                        AND IsGlassesLens = 0 )
        --    BEGIN
        --        DELETE  FROM @@TreatmentDetail
        --        WHERE   TreatmentDetailCode = 'TMD005'; --Frame
        --    END;

        --IF NOT EXISTS ( SELECT  1
        --                FROM    @@TempGlasses
        --                WHERE   IsGlassesFrame = 0
        --                        AND IsGlassesLens = 1 )
        --    BEGIN
        --        DELETE  FROM @@TreatmentDetail
        --        WHERE   TreatmentDetailCode = 'TMD006'; --Lens
        --    END;

        --IF NOT EXISTS ( SELECT  1
        --                FROM    @@TempGlasses
        --                WHERE   IsGlassesFrame = 1
        --                        AND IsGlassesLens = 1 )
        --    BEGIN
        --        DELETE  FROM @@TreatmentDetail
        --        WHERE   TreatmentDetailCode = 'TMD007'; --Frame + Lens
        --    END;
	
    END;

IF @@TreatmentCode = 'FP'
    BEGIN

        INSERT  INTO @@TreatmentDetail
                ( TreatmentDetail,
				  TreatmentDetailCode
                )
                SELECT  ISNULL(lt.LangTextDescription,
                               md.TreatmentDetailDescription) AS TreatmentDescription,
						md.TreatmentDetailCode
                FROM    Medcare.Treatment_Detail md
                        LEFT JOIN Medcare.Lang_Text lt ON lt.LangTextCode = md.LangTextCode
                                                          AND lt.LangModule = 'LGN001'
                                                          AND lt.LangID = @@LangCode
                WHERE   Status = 1
                        AND TreatmentCode = @@TreatmentCode;

        INSERT  INTO @@TempSex
                ( Sex
                )
                SELECT DISTINCT
                        Sex
                FROM    Policy P
                        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
                        INNER JOIN Policy_Member_Plan PMP ON PMP.PNo = P.PNO
                                                             AND PMP.MNo = PM.MNo
                                                             AND PMP.ExcludeF = 0
                        INNER JOIN Policy_Benefit_Schedule PBS ON PBS.PNO = P.PNO
                                                              AND PBS.PPlan = PMP.PPlan
                                                              AND PBS.BAmount = PMP.BAmount
                        INNER JOIN Benefit B ON PBS.BenefitID = B.BenefitID
                WHERE   PM.MemberNo = @@MemberNo
                        AND @@TreatmentStart BETWEEN PM.PDate AND PM.PPDate
                        AND PM.Membership IN ( '1. EMP', '2. SPO' );

        IF @@TreatmentStart = @@TreatmentFinish
            BEGIN
                DELETE  FROM @@TreatmentDetail
                WHERE   TreatmentDetailCode = 'TMD015';

                IF EXISTS ( SELECT  1
                                FROM    @@TempSex
                                WHERE   Sex = 'F' )
                    BEGIN
                        DELETE  FROM @@TreatmentDetail
                        WHERE   TreatmentDetailCode IN ( 'TMD014', 'TMD013' );
                    END;

                IF EXISTS ( SELECT  1
                                FROM    @@TempSex
                                WHERE   Sex = 'M' )
                    BEGIN
                        DELETE  FROM @@TreatmentDetail
                        WHERE   TreatmentDetailCode IN ( 'TMD008', 'TMD009',
                                                     'TMD010', 'TMD011',
                                                     'TMD012' );
                    END;
            END;

        IF @@TreatmentStart < @@TreatmentFinish
            BEGIN
                IF EXISTS ( SELECT  1
                                FROM    @@TempSex
                                WHERE   Sex = 'F' )
                    BEGIN
                        DELETE  FROM @@TreatmentDetail
                        WHERE   TreatmentDetailCode IN ( 'TMD008', 'TMD009',
                                                     'TMD010', 'TMD014',
                                                     'TMD012', 'TMD013' );
													 
                    END;

                IF EXISTS ( SELECT  1
                                FROM    @@TempSex
                                WHERE   Sex = 'M' )
                    BEGIN
                        DELETE  FROM @@TreatmentDetail
                        WHERE   TreatmentDetailCode IN ( 'TMD008', 'TMD009',
                                                     'TMD010', 'TMD012',
                                                     'TMD013', 'TMD011','TMD015'  );
                    END;
            END;
    END;

	 
IF @@TreatmentCode = 'DISP'
    BEGIN

        INSERT  INTO @@TreatmentDetail
                ( TreatmentDetail ,
                  TreatmentDetailCode
                )
                SELECT DISTINCT
                        ISNULL(lt.LangTextDescription,
                               td.TreatmentDetailDescription) AS TreatmentDescription,
                        td.TreatmentDetailCode
                FROM    Policy P
                        INNER JOIN Policy_Member PM ON P.PNO = PM.PNO
                        INNER JOIN Policy_Member_Plan PMP ON PMP.PNo = P.PNO
                                                             AND PMP.MNo = PM.MNo
                                                             AND PMP.ExcludeF = 0
                        INNER JOIN Policy_Benefit_Schedule PBS ON PBS.PNO = P.PNO
                                                              AND PBS.PPlan = PMP.PPlan
                                                              AND PBS.BAmount = PMP.BAmount
                        INNER JOIN Benefit B ON PBS.BenefitID = B.BenefitID
                        INNER JOIN Medcare.Mapping_Benefit_Treatment MPT ON MPT.BenefitID = PBS.BenefitID
                                                              AND MPT.Status = 1
                        INNER JOIN Medcare.Mst_Treatment mt ON mt.TreatmentCode = MPT.TreatmentCode
                                                              AND mt.Status = 1
                        INNER JOIN Medcare.Treatment_Detail td ON mt.TreatmentCode = td.TreatmentCode
                                                              AND td.Status = 1
						LEFT JOIN Medcare.Lang_Text lt ON lt.LangTextCode = td.LangTextCode
															AND lt.LangModule = 'LGN001'
															AND lt.LangID = @@LangCode
                WHERE   PM.MemberNo = @@MemberNo
                        AND @@TreatmentStart BETWEEN PM.PDate AND PM.PPDate
                        AND MPT.TreatmentCode = @@TreatmentCode

    END

SELECT  TreatmentDetail AS TreatmentDetailDescription, TreatmentDetailCode
FROM    @@TreatmentDetail ";
        #endregion

        #endregion

        #region 'Validate Reimbursement Treatment'
        internal static string qValidateReimbursementTreatment =
@";DECLARE @@MemberNo AS VARCHAR(100) = @0;
DECLARE @@TreatmentStart AS DATE = @1; --A/82756,B/00033623
DECLARE @@TreatmentCode AS VARCHAR(10)	= @2;
DECLARE @@TotalAmount AS DECIMAL = @3;
DECLARE @@LangCode AS VARCHAR(10) = @4
DECLARE @@TreatmentDetailCode AS VARCHAR(15) = @5
DECLARE @@RemainingAnnualLimit AS FLOAT;
DECLARE @@NextFrame DATETIME; 
DECLARE @@NextLens DATETIME;


IF OBJECT_ID('tempdb..#TempValidation') IS NOT NULL
    DROP TABLE #TempValidation;

IF OBJECT_ID('tempdb..#AvailableClaim') IS NOT NULL
    DROP TABLE	#AvailableClaim;

CREATE TABLE #TempValidation
    (
      ValidationCode VARCHAR(12),
	  ValidationValue VARCHAR(MAX)
    );

CREATE TABLE #AvailableClaim
    (
      NextAvailableClaimFrame DATETIME ,
      NextAvailableClaimLens DATETIME
    );

SELECT  *
INTO    #temp
FROM    fn_GetAccountInfo(@@MemberNo);

IF NOT EXISTS ( SELECT  1
                FROM    #temp )
    BEGIN 
        INSERT  INTO #TempValidation
                ( ValidationCode,
				  ValidationValue 
	            )
                SELECT  'STC00042',NULL
    END;
	
IF @@TreatmentCode = 'GL'
    BEGIN
        INSERT  INTO #AvailableClaim
                ( NextAvailableClaimFrame ,
                  NextAvailableClaimLens
	            )
                EXEC dbo.sp_GetGlassesNextAvailableClaim @@MemberNo; 

        IF EXISTS ( SELECT  1
                    FROM    #AvailableClaim )
            BEGIN
                SELECT TOP 1
                        @@NextFrame = NextAvailableClaimFrame ,
                        @@NextLens = NextAvailableClaimLens
                FROM    #AvailableClaim;

				IF @@TreatmentDetailCode = 'TMD005' --Frame
				BEGIN
					IF ( DATEDIFF(dd, CAST(@@NextFrame AS DATE),
                              CAST(@@TreatmentStart AS DATE)) < 0 )
                    BEGIN
                        INSERT  INTO #TempValidation
                                ( ValidationCode,
								  ValidationValue
					            )
                                SELECT  'STC00045',CONCAT('[""',FORMAT(@@NextFrame,'dd-MMM-yyyy') ,'""]');
										
                    END;
                END

				IF @@TreatmentDetailCode = 'TMD006' --Lensa
				BEGIN
					IF(DATEDIFF(dd, CAST(@@NextLens AS DATE),
                              CAST(@@TreatmentStart AS DATE)) < 0 )
                    BEGIN
                        INSERT  INTO #TempValidation
                                (ValidationCode,
                                  ValidationValue

                                )
                                SELECT  'STC00046',CONCAT('[""', FORMAT(@@NextLens,'dd-MMM-yyyy') ,'""]');
                    END;
				END
                

                IF(SELECT COUNT(1)FROM #TempValidation WHERE  ValidationCode IN ( 'STC00045', 'STC00046' )) > 1
                    BEGIN
                        DELETE FROM #TempValidation
                        WHERE ValidationCode IN( 'STC00045', 'STC00046' );

						INSERT INTO #TempValidation
                                (ValidationCode,
								 ValidationValue
								)
                                SELECT  'STC00047',CONCAT('[""', FORMAT(@@NextFrame,'dd-MMM-yyyy'),'"",','""',FORMAT(@@NextLens,'dd-MMM-yyyy'),'""]');
                    END;
            END;
        ELSE
            BEGIN
                INSERT INTO #TempValidation
                        (ValidationCode,
                          ValidationValue

                        )
                        SELECT  'STC00093',NULL
            END;
        END;

IF @@TreatmentCode <> 'GL'
    BEGIN
        SELECT DISTINCT
                PM.MemberNo ,
                PM.Name AS MemberName ,
                PT.ProductType AS ProductTypeID ,
                PT.Description AS ProductTypeName ,
                CASE WHEN L.FamilyPlanF = 0
                          AND L.IndividualAnnualLimit IN ( 999999999, 0 )
                     THEN -1
                     WHEN L.FamilyPlanF = 1
                          AND L.FamilyAnnualLimit IN ( 999999999, 0 ) THEN -1
                     WHEN L.FamilyPlanF = 0
                          AND L.IndividualAnnualLimit NOT IN ( 999999999, 0 )
                     THEN L.IndividualRemainingLimit
                     WHEN L.FamilyPlanF = 1
                          AND L.FamilyAnnualLimit NOT IN ( 999999999, 0 )
                     THEN L.FamilyRemainingLimit
                END AS RemainingAnnualLimit
        INTO    #temp1
        FROM    Policy_Member PM
                INNER JOIN Policy_Member_Plan PMP ON PMP.PNo = PM.PNO
                                                     AND PMP.MNo = PM.MNo
                                                     AND PMP.ExcludeF = 0
                INNER JOIN PPlan PP ON PP.PPlan = PMP.PPlan
                INNER JOIN Product PR ON PP.ProductID = PR.ProductID
                INNER JOIN ProductType PT ON PT.ProductType = PR.ProductType
                OUTER APPLY fn_GetLimitInfo(PM.MNo, PT.ProductType) L
        WHERE   PM.MemberNo = @@MemberNo
                AND @@TreatmentStart BETWEEN PM.PDate AND PM.PPDate
                AND PT.ProductType = @@TreatmentCode;
    

        IF EXISTS ( SELECT  1
                    FROM    #temp1 )
            BEGIN
				
                SELECT  @@RemainingAnnualLimit = RemainingAnnualLimit
                FROM    #temp1;

                IF ( @@RemainingAnnualLimit = 0 )
                    BEGIN
                        INSERT  INTO #TempValidation
                                ( ValidationCode ,
                                  ValidationValue

                                )
                                SELECT  'STC00043' ,CONCAT('[""',LangTextDescription, '""]') 
										FROM Medcare.Lang_Text 
										WHERE LangTextCode = @@TreatmentCode 
										AND LangID = @@LangCode
                    END
						ELSE 
							IF (CONVERT(DECIMAL, @@TotalAmount) > CONVERT(DECIMAL, @@RemainingAnnualLimit))
								AND @@RemainingAnnualLimit > 0
								BEGIN
								INSERT  INTO #TempValidation
                                ( ValidationCode ,
                                  ValidationValue

                                )

								SELECT 'STC00044',CONCAT('[""',LangTextDescription,'"",','""',CONCAT('Rp',REPLACE(FORMAT(@@RemainingAnnualLimit, '##,##0'),',','.')),'""]') 
										FROM Medcare.Lang_Text 
										WHERE LangTextCode = @@TreatmentCode 
										AND LangID = @@LangCode
								END
            END;
    END;

SELECT*
FROM    #TempValidation;

DROP TABLE #temp;

IF @@TreatmentCode <>'GL'
BEGIN
DROP TABLE #temp1;
END

DROP TABLE #TempValidation;
DROP TABLE #AvailableClaim ";
        #endregion

        #region 'GetTreatmentSymptomsList'
        internal static string qGetTreatmentSymptomsList = @"SELECT s.SymptomCode,
                                                                   COALESCE(lt.LangTextDescription, s.SymptomDescription) AS SymptomDescription
                                                            FROM Medcare.Symptom s
                                                                LEFT JOIN Medcare.Lang_Text lt
                                                                    ON s.LangTextCode = lt.LangTextCode
                                                                       AND lt.LangModule = 'LGN001'
                                                                       AND lt.LangID = @0
															WHERE s.Status = 1 AND s.RowStatus = 0";
        #endregion

        #region 'UpdateReimbursementTreatmentSymptoms'
        internal static string qUpdateReimbursementTreatmentSymptoms = @"UPDATE Medcare.Reimbursement_Transaction
                                                                            SET	 SymptomClaimData = @0,
                                                                            UpdateUsr = @1,
                                                                            UpdateDt = GETDATE()
                                                                            WHERE ClaimID = @2 AND ID = @3";

        #endregion

        #region 'GetReimbursementDetail'
        internal static string qGetReimbursementDetail = @"SELECT RT.MemberNo,
                                                                   PM.Name,
                                                                   'Rp' + REPLACE(FORMAT(RT.Nominal, '##,##0'),',','.') AS Nominal,
                                                                    CASE
                                                                        WHEN RT.TreatmentStart = RT.TreatmentEnd THEN
                                                                            CONVERT(VARCHAR(10), FORMAT(RT.TreatmentStart, 'dd/MM/yyyy'))
                                                                        ELSE
                                                                            CONVERT(VARCHAR(10), FORMAT(RT.TreatmentStart, 'dd/MM/yyyy')) + ' - '
                                                                            + CONVERT(VARCHAR(10), FORMAT(RT.TreatmentEnd, 'dd/MM/yyyy'))
                                                                    END AS TreatmentStart,
                                                                   COALESCE(lt.LangTextDescription, MT.TreatmentDescription) AS TreatmentDescription,
                                                                   RT.RelatedClaimNo,
                                                                   ISNULL(RT.ProviderName, '') AS ProviderName
                                                            FROM Medcare.Reimbursement_Transaction RT
                                                                INNER JOIN dbo.Policy_Member PM
                                                                    ON RT.MemberNo = PM.MemberNo
                                                                INNER JOIN Medcare.Mst_Treatment MT
                                                                    ON RT.TreatmentCode = MT.TreatmentCode
                                                                LEFT JOIN Medcare.Lang_Text lt
                                                                    ON MT.LangTextCode = lt.LangTextCode
                                                                       AND lt.LangModule = 'LGN001'
                                                            WHERE RT.ClaimID = @0
                                                                  AND RT.MemberNo = @1
                                                                  AND lt.LangID = @2
                                                                  AND (RT.TreatmentStart
                                                                  BETWEEN PM.PDate AND PM.PPDate
                                                                      );";
        #endregion

        #region 'GetAccountInfo'
        internal static string qGetAccountInfo = @"SELECT BankCode,
                                                   BankName,
                                                   BankBranch,
                                                   AccountNo,
                                                   AccountName,
                                                   Nationality,
                                                   AccountType FROM fn_GetAccountInfo(@0)";
        #endregion

        #region 'GetReimbursementFileTypeList'
        internal static string qGetReimbursementFileType = @";DECLARE @@TreatmentCode VARCHAR(25),
                                                                    @@TreatmentDetailCode VARCHAR(20),
                                                                    @@RelatedClaimNo VARCHAR(20),
                                                                    @@MemberNo VARCHAR(20)

                                                            SELECT @@TreatmentCode = TreatmentCode,
                                                                    @@TreatmentDetailCode = TreatmentDetailCode,
                                                                    @@RelatedClaimNo = RelatedClaimNo,
                                                                    @@MemberNo = MemberNo
                                                            FROM Medcare.Reimbursement_Transaction
                                                            WHERE ClaimID = @1 AND MemberNo = @2

                                                            CREATE TABLE #TempMapDocType
                                                            (
                                                                DocumentTypeID VARCHAR(5),
                                                                DocumentTypeName VARCHAR(200),
                                                                IsMandatory BIT,
                                                            );

                                                            INSERT INTO #TempMapDocType
                                                            (
                                                                DocumentTypeID,
                                                                DocumentTypeName,
                                                                IsMandatory
                                                            )
                                                            SELECT cdt.DocumentTypeID,
                                                                   COALESCE(lt.LangTextDescription, cdt.DocumentTypeName) AS DocumentTypeName,
                                                                   mtd.IsMandatory
                                                            FROM Medcare.Mapping_Treatment_DocumentType mtd
                                                                INNER JOIN dbo.ClaimDocumentType cdt
                                                                    ON cdt.DocumentTypeID = mtd.DocumentTypeID
                                                                LEFT JOIN Medcare.Lang_Text lt
                                                                    ON cdt.LangTextCode = lt.LangTextCode
                                                                       AND lt.LangModule = 'LGN001'
                                                                       AND lt.LangID = @0
                                                            WHERE mtd.TreatmentCode = @@TreatmentCode
                                                                  AND ISNULL(mtd.TreatmentDetailCode, '') = @@TreatmentDetailCode;

                                                            IF @@TreatmentCode = 'OP'
                                                               AND ISNULL(@@RelatedClaimNo, '') = ''
                                                            BEGIN
                                                                UPDATE #TempMapDocType
                                                                SET IsMandatory = 0
                                                                WHERE DocumentTypeID = 'INFDX';
                                                            END;

                                                            IF @@TreatmentCode = 'POSTIP'
                                                               AND @@RelatedClaimNo <> ''
                                                            BEGIN
                                                                DELETE FROM #TempMapDocType
                                                                WHERE DocumentTypeID = 'BKIP';
                                                            END;

                                                            IF @@TreatmentCode = 'POSTMA'
                                                               AND @@RelatedClaimNo <> ''
                                                            BEGIN
                                                                DELETE FROM #TempMapDocType
                                                                WHERE DocumentTypeID = 'BKMA';
                                                            END;

                                                            IF @@TreatmentCode = 'GL'
                                                            BEGIN
	                                                            IF EXISTS(SELECT 1
				                                                            FROM
					                                                            ClaimH CH 
					                                                            INNER JOIN Policy_Member PM ON CH.MNO = PM.MNO

				                                                            WHERE
					                                                            CH.ProductType = 'GL'
					                                                            AND CH.Status NOT IN ('X','C')
					                                                            AND PM.MemberNo = @@MemberNo )
	                                                            BEGIN
		                                                            IF @@TreatmentDetailCode IN ('TMD006', 'TMD007')
		                                                            BEGIN 
			                                                            DELETE FROM #TempMapDocType
			                                                            WHERE DocumentTypeID = 'SDGL';
		                                                            END	
		                                                            ELSE
		                                                            BEGIN
			                                                            DELETE FROM #TempMapDocType
			                                                            WHERE DocumentTypeID IN ('SDGL', 'SGLL');
		                                                            END
	                                                            END
	                                                            ELSE
	                                                            BEGIN
		                                                            DELETE FROM #TempMapDocType
		                                                            WHERE DocumentTypeID = 'SGLL';
	                                                            END	
                                                            END;

                                                            SELECT RTRIM(DocumentTypeID) AS DocumentTypeID ,
                                                                   DocumentTypeName,
                                                                   IsMandatory
                                                            FROM #TempMapDocType;
                                                            DROP TABLE #TempMapDocType;";
        #endregion

        #region 'UploadImage'
        internal static string qUploadImage = @"DECLARE @@imageID VARCHAR(100) = NEWID()
                                                ;INSERT INTO Medcare.Temp_Reimbursement_Image
	                                                (
                                                        ImageID,
	                                                    DeviceID,
	                                                    ImageType,
	                                                    ImageExt,
	                                                    PathFile,
	                                                    ClaimID,
	                                                    RowStatus,
	                                                    ImageData,
	                                                    ThumbnailData,
	                                                    IsMandatory,
	                                                    EntryUsr,
	                                                    EntryDt
	                                                )
	                                                VALUES
	                                                (   @@imageID,       -- ImageID - varchar(100)
	                                                    @0,       -- DeviceID - varchar(100)
	                                                    @1,       -- ImageType - char(6)
	                                                    @2,       -- ImageExt - varchar(6)
                                                        '',
	                                                    @3,       -- ClaimID - char(10)
	                                                    0,        -- RowStatus - smallint
	                                                    @4,     -- ImageData - varbinary(max)
	                                                    @5,     -- ThumbnailData - varbinary(max)
                                                        @6,     -- IsMandatory
	                                                    @7,       -- EntryUsr - char(5)
	                                                    GETDATE() -- EntryDt - datetime
	                                                    )
                                                     ;SELECT @@imageID";
        #endregion

        #region 'GetReimbursementFileExample'
        internal static string qGetReimbursementFileExample = @";SELECT ImageExampleCode,
                                                                       COALESCE(lt.LangTextDescription, rie.ImageExampleCaption) AS ImageExampleCaption,
                                                                       --rie.ImageExt,
                                                                       --rie.ImageData,
                                                                       --rie.ThumbnailData,
                                                                       rie.PathFile
                                                                FROM Medcare.Reimbursement_Image_Example rie
                                                                    LEFT JOIN Medcare.Lang_Text lt
                                                                        ON lt.LangTextCode = rie.LangTextCode
                                                                           AND lt.LangModule = 'LGN001'
                                                                           AND lt.LangID = @0
                                                                WHERE rie.Status = 1
                                                                      AND rie.RowStatus = 0
	                                                                  AND rie.ImageExampleCode = CASE WHEN @1 = '' THEN rie.ImageExampleCode ELSE @1 END
                                                                ORDER BY rie.Sequence";
        #endregion

        #region QueryGetSEAFullTextIndex
        internal static string QueryGetSEAFullTextIndexByID =
@";SELECT  
        ID ,
        DataID ,
        DataTerm ,
        DataType ,
        DataModule ,
        DataDescription,
        RowStatus
FROM    SEAFullTextIndex
WHERE   ID = @0
        AND DataType = @1
        AND DataModule = 'MEDCARE'";
        #endregion

        #region QueryGetSEAFullTextIndexBySearchKeyword
        internal static string QueryGetSEAFullTextIndexBySearchKeyword =
@";exec usp_SEAFullTextSearch 
@Keywords  = @0
@DataType  = @1
@DataModule = 'MEDCARE'";
        #endregion

        #region QueryGetMedcareClaimID
        internal static string QueryGetMedcareClaimID =
@";exec sp_GetMedcareClaimID";
        #endregion

        #region QueryGetReimbursementClaimID
        internal static string QueryGetReimbursementClaimID =
@";SELECT  rt.ClaimID
    FROM    Medcare.Reimbursement_Transaction rt WITH ( NOLOCK )
    WHERE   rt.ClaimNo = @0
            AND ISNULL(rt.ClaimNo, '') <> ''";
        #endregion

        #region 'Delete Image'
        internal static string qDeleteImage = @"UPDATE Medcare.Reimbursement_Image
                                                SET RowStatus = 1, UpdateUsr = @1, UpdateDt = GETDATE()
                                                WHERE ReimbursementImageID = @0
                                                SELECT 1";
        #endregion

        #region 'Submit Reimbursement'
        internal static string qSubmitReimbursement = @"DECLARE @@StatusBefore CHAR(1);

                                                SELECT @@StatusBefore = Status
                                                FROM Medcare.Reimbursement_Transaction
                                                WHERE ClaimID = @0
                                                        AND MemberNo = @1;

                                                UPDATE rt
                                                SET rt.Status = @5,
                                                    rt.EntryUsr = @2,
                                                    rt.UpdateDt = GETDATE(),
                                                    rt.SymptomClaimData = @3
                                                FROM Medcare.Reimbursement_Transaction rt
                                                WHERE ClaimID = @0
                                                      AND MemberNo = @1
                                                      AND Status = @4;

                                                INSERT INTO Medcare.Hst_Reimbursement_Transaction
                                                (
                                                    ClaimID,
                                                    Status_Before,
                                                    Status_After,
                                                    Remarks,
                                                    RowStatus,
                                                    EntryUsr,
                                                    EntryDt
                                                )
                                                SELECT ClaimID,
                                                       @@StatusBefore,
                                                       Status,
                                                       'Submit Reimbursement',
                                                       0,
                                                       @2,
                                                       GETDATE()
                                                FROM Medcare.Reimbursement_Transaction
                                                WHERE ClaimID = @0
                                                      AND MemberNo = @1;

                                                DECLARE @@TblTmpImg TABLE
                                                (
                                                    ImageID VARCHAR(100)
                                                );

                                                INSERT INTO @@TblTmpImg
                                                SELECT mtri.ImageID
                                                FROM Medcare.Reimbursement_Transaction mrt
                                                    INNER JOIN Medcare.Temp_Reimbursement_Image mtri
                                                        ON mtri.ClaimID = mrt.ClaimID
                                                    LEFT JOIN Medcare.Reimbursement_Image mri
                                                        ON mri.ClaimID = mtri.ClaimID
                                                           AND mri.ImageID = mtri.ImageID
                                                WHERE mrt.ClaimID = @0
                                                      AND mrt.MemberNo = @1
                                                      AND mri.ImageID IS NULL
                                                      AND mtri.RowStatus = 0;


                                                INSERT INTO Medcare.Reimbursement_Image
                                                (
                                                    ReimbursementImageID,
                                                    ClaimID,
                                                    ImageID,
                                                    DeviceID,
                                                    ImageType,
                                                    ImageExt,
                                                    PathFile,
                                                    RowStatus,
                                                    ImageData,
                                                    ThumbnailData,
                                                    IsMandatory,
                                                    EntryUsr,
                                                    EntryDt
                                                )
                                                SELECT NEWID(),
                                                       mtri.ClaimID,
                                                       mtri.ImageID,
                                                       mtri.DeviceID,
                                                       mtri.ImageType,
                                                       mtri.ImageExt,
                                                       mtri.PathFile,
                                                       0,
                                                       mtri.ImageData,
                                                       mtri.ThumbnailData,
                                                       mtri.IsMandatory,
                                                       @2,
                                                       GETDATE()
                                                FROM Medcare.Temp_Reimbursement_Image mtri
                                                    INNER JOIN @@TblTmpImg ti
                                                        ON ti.ImageID = mtri.ImageID;

                                                UPDATE mtri
                                                SET mtri.RowStatus = 1
                                                FROM Medcare.Temp_Reimbursement_Image mtri
                                                    INNER JOIN @@TblTmpImg ti
                                                        ON ti.ImageID = mtri.ImageID;

                                                SELECT 1";
        #endregion

        #region 'RE-Submit Reimbursement'
        internal static string qResubmitReimbursement = @"DECLARE @@StatusBefore CHAR(1);

                                                SELECT @@StatusBefore = Status
                                                FROM Medcare.Reimbursement_Transaction
                                                WHERE ClaimID = @0
                                                        AND MemberNo = @1;

                                                UPDATE rt
                                                SET rt.Status = @4,
                                                    rt.EntryUsr = @2,
                                                    rt.UpdateDt = GETDATE()
                                                FROM Medcare.Reimbursement_Transaction rt
                                                WHERE ClaimID = @0
                                                        AND MemberNo = @1
                                                        AND Status = @3;

                                                INSERT INTO Medcare.Hst_Reimbursement_Transaction
                                                (
                                                    ClaimID,
                                                    Status_Before,
                                                    Status_After,
                                                    Remarks,
                                                    RowStatus,
                                                    EntryUsr,
                                                    EntryDt
                                                )
                                                SELECT ClaimID,
                                                       @@StatusBefore,
                                                       Status,
                                                       'Submit Reimbursement',
                                                       0,
                                                       @2,
                                                       GETDATE()
                                                FROM Medcare.Reimbursement_Transaction
                                                WHERE ClaimID = @0
                                                      AND MemberNo = @1;

                                                DECLARE @@TblTmpImg TABLE
                                                (
                                                    ImageID VARCHAR(100),
                                               ImageType VARCHAR(100)
                                                );

                                                INSERT INTO @@TblTmpImg
                                                SELECT mtri.ImageID, mtri.ImageType
                                                FROM Medcare.Reimbursement_Transaction mrt
                                                    INNER JOIN Medcare.Temp_Reimbursement_Image mtri
                                                        ON mtri.ClaimID = mrt.ClaimID
                                                    LEFT JOIN Medcare.Reimbursement_Image mri
                                                        ON mri.ClaimID = mtri.ClaimID
                                                           AND mri.ImageID = mtri.ImageID
                                                WHERE mrt.ClaimID = @0
                                                      AND mrt.MemberNo = @1
                                                      AND mri.ImageID IS NULL
                                                      AND mtri.RowStatus = 0;

                                                INSERT INTO medcare.Reimbursement_Image
                                                (
                                                    ReimbursementImageID,
                                                    ClaimID,
                                                    ImageID,
                                                    DeviceID,
                                                    ImageType,
                                                    ImageExt,
                                                    PathFile,
                                                    RowStatus,
                                                    ImageData,
                                                    ThumbnailData,
                                                    IsMandatory,
                                                    EntryUsr,
                                                    EntryDt
                                                )
                                                SELECT NEWID(),
                                                       mtri.ClaimID,
                                                       mtri.ImageID,
                                                       mtri.DeviceID,
                                                       mtri.ImageType,
                                                       mtri.ImageExt,
                                                       mtri.PathFile,
                                                       0,
                                                       mtri.ImageData,
                                                       mtri.ThumbnailData,
                                                       mtri.IsMandatory,
                                                       @2,
                                                       GETDATE()
                                                FROM Medcare.Temp_Reimbursement_Image mtri
                                                    INNER JOIN @@TblTmpImg ti
                                                        ON ti.ImageID = mtri.ImageID;

                                                UPDATE mri
                                                SET mri.Validity = NULL,
                                                    mri.ValidityDetail = NULL
                                                FROM Medcare.Reimbursement_Image mri
                                                WHERE mri.ClaimID = @0
                                                      AND mri.ImageType IN
                                                          (
                                                              SELECT DISTINCT TI.ImageType FROM @@TblTmpImg TI
                                                          );

                                                UPDATE mtri
                                                SET mtri.RowStatus = 1
                                                FROM Medcare.Temp_Reimbursement_Image mtri
                                                    INNER JOIN @@TblTmpImg ti
                                                        ON ti.ImageID = mtri.ImageID;

                                                SELECT 1";
        #endregion

        #region 'Get Tier By MemberNo'
        internal static string qCheckNeedHardCopy = @"DECLARE @@Billed FLOAT = @1,
		                                                    @@MemberNo VARCHAR(20) = @0,
		                                                    @@NumOfClaimPeriod INT = (SELECT value
			                                                    FROM Config_Parameters
			                                                    WHERE [key] = 'NumOfClaimPeriod'
				                                                        AND category = 'G5MEDCARE')


                                                    IF EXISTS
                                                    (
                                                        SELECT TOP 1
                                                                1
                                                        FROM MedcareInvoiceMandatory
                                                        WHERE @@Billed
                                                                BETWEEN MinBilled AND MaxBilled
                                                                AND IsActive = 1
                                                    )
                                                    BEGIN

                                                        DECLARE @@TieringMinBilled FLOAT;
                                                        DECLARE @@TieringMaxBilled FLOAT;
                                                        DECLARE @@TieringNumOfClaim INT;
                                                        DECLARE @@CountPrevClaim INT = 0;

                                                        SELECT TOP 1
                                                                @@TieringMinBilled = MinBilled,
                                                                @@TieringMaxBilled = MaxBilled,
                                                                @@TieringNumOfClaim = NumOfClaim
                                                        FROM MedcareInvoiceMandatory
                                                        WHERE @@Billed
                                                                BETWEEN MinBilled AND MaxBilled
                                                                AND IsActive = 1;

                                                        SELECT @@CountPrevClaim = COUNT(1)
                                                        FROM ClaimH CH
                                                            INNER JOIN Policy_Member PM
                                                                ON CH.MNO = PM.MNo
                                                            INNER JOIN Medcare.Reimbursement_Transaction RT
                                                                ON CH.ClaimNo = RT.ClaimNo
                                                                    AND CAST(RT.EntryDt AS DATE) >= DATEADD(MONTH, @@NumOfClaimPeriod * -1, CAST(RT.TreatmentStart AS DATE))
                                                        WHERE PM.MemberNo = @@MemberNo
                                                                AND CH.Billed
                                                                BETWEEN @@TieringMinBilled AND @@TieringMaxBilled
                                                                AND
                                                                (
                                                                    CH.Status = 'R'
                                                                    OR RT.Status = 'H'
                                                                );

                                                        IF (@@CountPrevClaim + 1) >= @@TieringNumOfClaim
                                                        BEGIN
                                                            SELECT 1;
                                                        END;
                                                        ELSE
                                                        BEGIN
                                                            SELECT 0;
                                                        END;
                                                    END;";
        #endregion

        #region 'Delete Temp Image'
        internal static string qDeleteTempImage = @"UPDATE Medcare.Temp_Reimbursement_Image
                                                SET RowStatus = 1
                                                WHERE ImageID = @0
                                                SELECT 1";
        #endregion

        #region 'Delete Temp Image By ClaimID'
        internal static string qDeleteTempImageByClaim = @"UPDATE Medcare.Temp_Reimbursement_Image
                                                SET RowStatus = 1
                                                WHERE ClaimID = @0
                                                SELECT 1";
        #endregion

    }
}