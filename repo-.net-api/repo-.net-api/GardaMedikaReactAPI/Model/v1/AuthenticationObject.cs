﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static GardaMedikaReactAPI.Model.v1.SEAObject;

namespace GardaMedikaReactAPI.Model.v1
{
    public class AuthenticationObject
    {
        public AuthenticationObject() { }
        
        public AuthenticationObject(string authKey, string  encryptedAuthKey)
        {
            this.AuthKey = authKey;
            this.EncryptedAuthKey = encryptedAuthKey;
        }
        
        public string AuthKey { get; set; }

        public string EncryptedAuthKey { get; set; }

        public string DeviceID { get; set; }

        public string MemberNo { get; set; }

        public string CustomerID { get; set; }

        public string ValidFrom { get; set; }

        public string ValidTo { get; set; }
        
        public string Module { get; set; }

        public string LangID { get; set; }

        public string AnonymousID { get; set; }

        public List<AuthenticationParameter> AuthorizationParameter{ get; set; }

    }

    public class AuthenticationParameter
    {
        public AuthenticationParameter() { }
        public AuthenticationParameter(string ParameterName, string ParameterValue)
            { this.ParameterName = ParameterName; this.ParameterValue = ParameterValue; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
    }
    
    public class AuthenticationResponse
    {
        public Boolean Status { get; set; }
        public int ErrorCode { get; set; }
        public String Token { get; set; }
        public int ValidHours { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public String Message { get; set; }
        public String WaitingDelay { get; set; }
        public String LangID { get; set; }
        public String RoutePath { get; set; }
        public string DeviceID { get; set; }
        public string MemberNo { get; set; }
        public string CustomerID { get; set; }
        public string ClaimID { get; set; }
        public List<LanguageStaticText> LanguageStaticText {get; set;}
        public List<Symptom> SymptomList { get; set; }
        public List<MemberInfo> MemberInfo { get; set; }
        public decimal MaxAmountBilled { get; set; }

        public AuthenticationResponse() { }

        public AuthenticationResponse(bool Status, int ErrorCode, String Token, int ValidHours)
        {
            this.Status = Status;
            this.ErrorCode = ErrorCode;
            this.Token = Token;
            this.ValidHours = ValidHours;
        }
    }
}