﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GardaMedikaReactAPI.Model.v1
{
    public class ApplicationOption
    {
        public string ApplicationName { get; set; }
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }

    public class GeneralParameter
    {
        public GeneralParameter() { }
        public GeneralParameter(string ID, string Description, int RowStatus = 0)
        {
            this.ID = ID;
            this.Description = Description;
            this.RowStatus = RowStatus;
        }
        public string ID { get; set; }
        public string Description { get; set; }
        public int RowStatus { get; set; }
    }

    public class EmailParameter
    {
        public EmailParameter() { }
        public EmailParameter(string ParamID, string ParamValue)
        {
            this.ParamID = ParamID;
            this.ParamValue = ParamValue;
        }
        public string ParamID { get; set; }
        public string ParamValue { get; set; }
    }

    public class LanguageStaticText
    {
        public LanguageStaticText() { }
        public string LangTextCode { get;set; }
        public string LangID { get; set; }
        public string LangTextDescription { get; set; }
    }
}