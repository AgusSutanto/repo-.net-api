﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GardaMedikaReactAPI.Model.v1
{
    public class MedcareObject
    {
        public string MemberNo { get; set; }
        public string DeviceID { get; set; }
        public string ClientDeviceID { get; set; }
        public string CustomerID { get; set;}
    }
}