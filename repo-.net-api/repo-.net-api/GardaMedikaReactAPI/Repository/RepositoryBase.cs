﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Reflection;

namespace GardaMedikaReactAPI.Repository
{
    internal class RepositoryBase
    {
        protected static readonly a2isLogHelper _logger = new a2isLogHelper();

        #region Connection String
        protected const string a2isHealthDB = "a2isHealthDB";
        protected const string a2isMedcareDB = "a2isMedcareDB";
        protected const string a2isGardaMobileCMS = "a2isGardaMobileCMSDB";
        #endregion


        #region Get Connection String
        internal static a2isDBHelper.Database GetA2isHealthDB()
        {
            return new a2isDBHelper.Database(a2isHealthDB);
        }
        internal static a2isDBHelper.Database GetA2isMedcareDB()
        {
            return new a2isDBHelper.Database(a2isMedcareDB);
        }
        internal static a2isDBHelper.Database GetA2isGardaMobileCMSDB()
        {
            return new a2isDBHelper.Database(a2isGardaMobileCMS);
        }
        #endregion

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            MethodBase method = sf.GetMethod();

            return string.Format("{0}.{1}", method.DeclaringType, method.Name);
        }
    }
}