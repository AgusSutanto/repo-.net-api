﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;
using System.Security;
using a2is.Framework.Security;
using System.Web.Http.Filters;
using System.Reflection;
using System.Web.Http.ModelBinding;
using System.ComponentModel.DataAnnotations;
using GardaMedikaReactAPI.Model.v1;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Repository;
using Newtonsoft.Json;

namespace GardaMedikaReactAPI.Repository.v1
{
    internal class Authentication : RepositoryBase
    {
        internal static AuthenticationResponse AuthenticateMedcareSignature(ref AuthenticationObject auth)
        {
            string fn = GetCurrentMethod();

            _logger.Debug(LogFormat.BeginMsgInfo(fn,JsonConvert.SerializeObject(auth)));

            AuthenticationResponse authResult = new AuthenticationResponse(false, -1, null, 0);

            try
            {
                int hours;
                var helper = new a2is.Framework.Security.a2isJWTHelper();

                if (!String.IsNullOrWhiteSpace(auth.AnonymousID) && auth != null)
                {
                    var token = helper.CreateAnonymToken(auth.AnonymousID, DateTime.Now, out hours);
                                       
                    authResult = new AuthenticationResponse(true, 0x0, token, hours)
                    {
                        EntryDate = DateTime.Now,
                        LastUpdatedTime = DateTime.Now,
                        LangID = auth.LangID
                    };

                    authResult.LanguageStaticText = ApplicationDBParameter.GetStaticLanguageContent(auth.LangID??ApplicationSettingAccess.GetAppConfig("DefaultLanguageCode"));
                    var route = auth.AuthorizationParameter.Find(x => x.ParameterName == "Path");                    
                    authResult.RoutePath = route == null ? "" : route.ParameterValue;
                    authResult.DeviceID = auth.DeviceID;
                    authResult.MemberNo = auth.MemberNo;
                    authResult.CustomerID = auth.CustomerID;
                    authResult.MemberInfo = GardaMedikaAccess.GetMemberInfo(auth.MemberNo);
                    authResult.SymptomList = GardaMedikaAccess.GetTreatmentSymptomsList(auth.LangID ?? ApplicationSettingAccess.GetAppConfig("DefaultLanguageCode"));                    
                    decimal maxAmountBilled = 0;
                    Decimal.TryParse(ApplicationDBParameter.GetAppParamValueGen5Health("MAX-AMOUNT-BILLED"), out maxAmountBilled);
                    authResult.MaxAmountBilled = maxAmountBilled;

                    var claimID = auth.AuthorizationParameter.Find(x => x.ParameterName == "ClaimID");                       
                    if (claimID == null)
                    {
                        authResult.ClaimID = SEADataAccess.GetMedcareClaimID();
                        auth.AuthorizationParameter.Add(new AuthenticationParameter("ClaimID", authResult.ClaimID));
                    }
                    else if (String.IsNullOrWhiteSpace(claimID.ParameterValue))
                    {
                        authResult.ClaimID = SEADataAccess.GetMedcareClaimID();
                        claimID.ParameterValue = authResult.ClaimID;
                    }
                    else
                    {
                        authResult.ClaimID = claimID == null ? String.Empty : SEADataAccess.GetReimbursementClaimID(claimID.ParameterValue);
                    }

                    if (!String.IsNullOrWhiteSpace(authResult.Token))
                        GardaMobileCMSAccess.UpdateMedcareLogClaimID(auth, authResult);
                }
                else if (String.IsNullOrEmpty(auth.Module) && !auth.Module.In(Modules.REIMBURSEMENT,Modules.REUPLOAD_REIMBURSEMENT))
                {
                    _logger.Error(LogFormat.ErrorMsgInfo(fn, String.Format("{0}{1}", "Invalid Modules : ", auth.Module)));
                }
                else
                {
                    _logger.Error(LogFormat.ErrorMsgInfo(fn, String.Format("{0}{1}","Invalid Anonymous ID : ",auth.AnonymousID)));
                }
            }
            catch (Exception e)
            {
                _logger.Error(LogFormat.ErrorMsgInfo(fn,e.ToString()));
            }

            return authResult;
        }
    }

    internal sealed class ReactMedcareAESHelper
    {
        private a2isAESHelper ReactMedcareSignatureAESHelper, ReactMedcareGuidAESHelper;

        private string ReactMedcareSignatureSecretKey = ApplicationDBParameter.GetAppParamValueGen5Health("MEDCARE-SECRET-KEY");
        private string ReactMedcareGuidSecretKey = ApplicationDBParameter.GetAppParamValueGen5Health("MEDCARE-GUID-SECRET-KEY");

        private static readonly ReactMedcareAESHelper _instance = new ReactMedcareAESHelper();

        public static ReactMedcareAESHelper Instances
        {
            get { return _instance; }
        }

        private ReactMedcareAESHelper()
        {
            ReactMedcareSignatureAESHelper = new a2isAESHelper(
                String.IsNullOrWhiteSpace(ReactMedcareSignatureSecretKey) ?
                ApplicationSettingAccess.GetAppConfig("ReactMedcareSecretKey") : ReactMedcareSignatureSecretKey);

            ReactMedcareGuidAESHelper =
                new a2isAESHelper(
                String.IsNullOrWhiteSpace(ReactMedcareGuidSecretKey) ?
                ApplicationSettingAccess.GetAppConfig("ReactMedcareSecretKey") : ReactMedcareGuidSecretKey);
        }

        internal string SignatureEncrypt(string data)
        {
            return ReactMedcareSignatureAESHelper.Encrypt(data);
        }

        internal string SignatureDecrypt(string encrypted)
        {
            return ReactMedcareSignatureAESHelper.Decrypt(encrypted);
        }

        internal string GuidEncrypt(string data)
        {
            return ReactMedcareGuidAESHelper.Encrypt(data);
        }

        internal string GuidDecrypt(string encrypted)
        {
            return ReactMedcareGuidAESHelper.Decrypt(encrypted);
        }
    }
}