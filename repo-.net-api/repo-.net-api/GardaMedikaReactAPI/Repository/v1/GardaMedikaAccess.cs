﻿using a2is.Framework.Monitoring;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Model.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static GardaMedikaReactAPI.Model.v1.SEAObject;

namespace GardaMedikaReactAPI.Repository.v1
{
    internal class GardaMedikaAccess
    {
        #region Logger
        private static readonly a2isLogHelper _logger = new a2isLogHelper();
        #endregion

        internal static List<MemberInfo> GetMemberInfo(string MemberNo)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<MemberInfo> srcResult = new List<MemberInfo>();
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<MemberInfo>(QueryCollection.qGetMemberInfo, MemberNo);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static List<TreatmentType> GetTreatmentType(string MemberNo, string TreatmentStart, string TreatmentFinish, string LangCode)
        {
            #region testing
            /*
             *SET @@MemberNo = 'A/82756';
             *SET @@TreatmentStart = '2019-02-02';
             */
            #endregion

            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<TreatmentType> srcResult = new List<TreatmentType>();
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<TreatmentType>(QueryCollection.qGetTreatmentType, MemberNo, TreatmentStart, TreatmentFinish, LangCode);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }
            return srcResult;
        }

        internal static List<TreatmentTypeDetail> GetTreatmentTypeDetail(List<string> Param)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<TreatmentTypeDetail> srcResult = new List<TreatmentTypeDetail>();
            try
            {
                #region Testing
                //SET @@MemberNo = 'F/00036160';
                //SET @@TreatmentStart = '2019-feb-02';
                //SET @@TreatmentFinish = '2019-feb-02';
                //SET @@TreatmentType = 'DISP';
                #endregion

                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<TreatmentTypeDetail>(QueryCollection.qGetTreatmentTypeDetail, Param.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }
            return srcResult;
        }

        internal static List<ValidationResult> ValidateReimbursementTreatment(ReimbursementData Param, string LangID)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<ValidationResult> vldResult = new List<ValidationResult>();
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    vldResult = db.Fetch<ValidationResult>(QueryCollection.qValidateReimbursementTreatment,
                        Param.MemberNo, Param.TreatmentStart, Param.TreatmentCode.Trim(), 
                        Param.AmountBilled, LangID, Param.TreatmentDetailCode.Trim());
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }
            return vldResult;
        }

        internal static List<Symptom> GetTreatmentSymptomsList(string LanguageCode)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<Symptom> srcResult = new List<Symptom>();

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<Symptom>(QueryCollection.qGetTreatmentSymptomsList, LanguageCode);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static ReimbursementDetail GetReimbursementDetail(string ClaimID, string MemberNo, string LangCode)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            ReimbursementDetail srcResult = new ReimbursementDetail();

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    List<ReimbursementDetail> srcResults = db.Fetch<ReimbursementDetail>(QueryCollection.qGetReimbursementDetail, ClaimID, MemberNo, LangCode);
                    if (srcResults.Count > 0)
                    {
                        srcResult = srcResults[0];
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static AccountInfo GetAccountInfo(string MemberNo)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            AccountInfo srcResult = new AccountInfo()
            {
                AccountName = "",
                AccountNo = "",
                AccountType = "",
                BankBranch = "",
                BankCode = "",
                BankName = "",
                Nationality = ""
            };

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    List<AccountInfo> srcResults = db.Fetch<AccountInfo>(QueryCollection.qGetAccountInfo, MemberNo);
                    if (srcResults.Count > 0)
                    {
                        srcResult = srcResults[0];
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static List<PrepostClaim> ReimbursementPrepostData(List<string> Param)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<PrepostClaim> srcResult = new List<PrepostClaim>();

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<PrepostClaim>(QueryCollection.qGetReimbursementPrepostData, Param.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static bool UpdateReimbursementPrepost(string RelatedClaimNo, string ClaimID, string MemberNo, string TreatmentCode)
        {
            var result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.Execute(QueryCollection.qUpdateReimbursementPrepost, RelatedClaimNo, ClaimID, MemberNo, TreatmentCode);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }
            return result > 0;
        }

        internal static List<ReimbursementFileType> GetReimbursementFileTypeList(string LangCode, string ClaimID, string MemberNo)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<ReimbursementFileType> srcResult = new List<ReimbursementFileType>();
            List<ReimbursementFileImage> srcResultImages = new List<ReimbursementFileImage>();

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<ReimbursementFileType>(QueryCollection.qGetReimbursementFileType, LangCode, ClaimID, MemberNo);
                    srcResultImages = db.Fetch<ReimbursementFileImage>(QueryCollection.qGetReimbursementFileImage, ClaimID);
                    if (srcResultImages.Count > 0)
                    {
                        foreach (ReimbursementFileType item in srcResult)
                        {
                            string fileType = item.DocumentTypeID;
                            List<ReimbursementFileImage> fileImage = srcResultImages.Where(p => p.ImageType == fileType).ToList();
                            item.Images = fileImage;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static bool ProcessReimbursementData(ReimbursementData Param, string UserID)
        {
            #region test
            /* --------SQL-----------
             * @0 , -- ClaimID - char(10)
             * @1 , -- MemberNo - varchar(20)
             * GETDATE() , -- TreatmentStart - datetime
             * GETDATE() , -- TreatmentEnd - datetime
             * @2 , -- TreatmentCode - char(6)
             * @3 , -- TreatmentDetailCode - char(6)
             * @4 , -- Nominal
             * @5 , -- DeviceID - varchar(200)
             * @6 , -- Status - smallint
             * 0 , -- RowStatus - smallint
             * @7 , -- EntryUsr - char(5)
             * GETDATE()  -- UpdateDt - datetime
             */
            #endregion

            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());

            var result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {

                    ProviderInfoByDescription provider = GetProviderInfoByDescription(Param.TreatmentPlace);

                    result = db.Execute(QueryCollection.qProcessReimbursementData,
                                        Param.ClaimID,
                                        Param.MemberNo,
                                        Param.TreatmentStart,
                                        Param.TreatmentEnd,
                                        Param.TreatmentCode,
                                        Param.TreatmentDetailCode, 
                                        provider == null ? "" : provider.ID, 
                                        Param.TreatmentPlace,
                                        Param.AmountBilled, 
                                        Param.DeviceID, 
                                        Param.Status,
                                        UserID, 
                                        Param.MemberNoLogin);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return false;
            }

            return result > 0;
        }

        internal static ProviderInfoByDescription GetProviderInfoByDescription(string TreatmentPlace)
        {
            ProviderInfoByDescription result = null;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    string baseQuery = QueryCollection.qGetProviderInfoByDesc;
                    baseQuery = baseQuery.Replace("/*DataDescription*/", " AND DataDescription = @0 ");
                    result = db.Fetch<ProviderInfoByDescription>(baseQuery, TreatmentPlace).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                result = null;
            }
            return result;
        }

        internal static bool SubmitReimbursementData(List<string> Param)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            var result = 0; var result1 = 0;

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.Execute(QueryCollection.qUpdateReimbursementTransaction, Param.ToArray());
                    if (result > 0)
                    {
                        result1 = db.Execute(QueryCollection.qInsertReimbursementImage, Param[4]);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return false;
            }

            return result > 0 && result1 > 0;
        }

        internal static ReimbursementData GetReimbursementData(string ClaimID, bool IncludeFile = false)
        {
            ReimbursementData result = null;

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.Fetch<ReimbursementData>(QueryCollection.qGetReimbursementData, ClaimID).FirstOrDefault();
                    if (IncludeFile)
                        result.File = db.Fetch<ReimbursementFileImage>(QueryCollection.qGetReimbursementFileImage, ClaimID);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }
            return result;
        }

        internal static string UploadImage(ReimbursementFileImage Param)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            string result = string.Empty;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.ExecuteScalar<string>(QueryCollection.qUploadImage, Param.DeviceID, Param.ImageType, Param.ImageExt,
                            Param.ClaimID, Param.ImageData, Param.ThumbnailData, Param.IsMandatory, Param.EntryUsr);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
                return result;
            }

            return result;

        }

        internal static bool CheckIsReimbursementNeedSymptoms(string RelatedClaimNo)
        {
            int result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.ExecuteScalar<int>(QueryCollection.qCheckIsReimbursementNeedSymptoms, RelatedClaimNo);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return result > 0;
        }

        internal static bool DeleteImage(string ReimbursementImageID, string UserID)
        {
            int result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.ExecuteScalar<int>(QueryCollection.qDeleteImage, ReimbursementImageID, UserID);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return result > 0;
        }
        internal static bool SubmitReimbursement(string ClaimID, string MemberNo, string UserID, string SymptomList, string RequiredStatus, string UpdatedStatus)
        {
            int result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.ExecuteScalar<int>(QueryCollection.qSubmitReimbursement, ClaimID, MemberNo, UserID, SymptomList, RequiredStatus, UpdatedStatus);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return result > 0;
        }

        internal static bool ResubmitReimbursement(string ClaimID, string MemberNo, string UserID, string RequiredStatus, string UpdatedStatus)
        {
            int result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.Execute(QueryCollection.qResubmitReimbursement, ClaimID, MemberNo, UserID, RequiredStatus, UpdatedStatus);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return result > 0;
        }

        internal static List<ReimbursementImageExample> GetReimbursementFileExample(string ImageExampleCode, string LangCode)
        {
            _logger.Debug(ErrorHandling.GetFunctionBeginMessage());
            List<ReimbursementImageExample> srcResult = new List<ReimbursementImageExample>();

            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    srcResult = db.Fetch<ReimbursementImageExample>(QueryCollection.qGetReimbursementFileExample, LangCode, ImageExampleCode);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return srcResult;
        }

        internal static bool CheckNeedHardCopy(string MemberNo, int amountBilled)
        {
            int result = 0;
            try
            {
                //string baseQuery = string.Format(QueryCollection.qCheckNeedHardCopy, string.Join("','", ReimbursementStatus));
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    result = db.ExecuteScalar<int>(QueryCollection.qCheckNeedHardCopy, MemberNo, amountBilled);
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return result == 1;
        }

        internal static bool DeleteTempImage(string ImageID, string claimID)
        {
            int result = 0;
            try
            {
                using (var db = RepositoryBase.GetA2isHealthDB())
                {
                    if (!string.IsNullOrEmpty(claimID))
                    {
                        result = db.ExecuteScalar<int>(QueryCollection.qDeleteTempImage, claimID);
                    }
                    else
                    {
                        result = db.ExecuteScalar<int>(QueryCollection.qDeleteTempImage, ImageID);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(ErrorHandling.GetFunctionErrorMessage(Errors.UnhandledException, e.ToString()));
            }

            return result > 0;
        }
    }
}