﻿using a2is.Framework.Monitoring;
using GardaMedikaReactAPI.General.v1;
using GardaMedikaReactAPI.Model.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static GardaMedikaReactAPI.Model.v1.SEAObject;

namespace GardaMedikaReactAPI.Repository.v1
{
    internal class SEADataAccess
    {
        #region Logger
        private static readonly a2isLogHelper _logger = new a2isLogHelper();
        #endregion


        internal static SEAFullTextIndex GetSEAFullTextIndexDataByID(int fullTextID, string dataType)
        {
            List<SEAFullTextIndex> returnData = new List<SEAFullTextIndex>();

            using (var db = RepositoryBase.GetA2isHealthDB())
            {
                returnData = db.Fetch<SEAFullTextIndex>
                    (
                        QueryCollection.QueryGetSEAFullTextIndexByID,
                        fullTextID,
                        dataType
                    ).ToList<SEAFullTextIndex>();
            }

            return returnData.Any() ? returnData.First() : null;
        }


        internal static List<SEAFullTextIndex> GetSEAFullTextIndexData(string keyword, string dataType)
        {
            List<SEAFullTextIndex> returnData = new List<SEAFullTextIndex>();

            using (var db = RepositoryBase.GetA2isHealthDB())
            {
                returnData = db.Fetch<SEAFullTextIndex>
                    (
                        QueryCollection.QueryGetSEAFullTextIndexBySearchKeyword,
                        keyword,
                        dataType
                    ).ToList<SEAFullTextIndex>();
            }

            return returnData;
        }

        internal static List<GeneralParameter> GetProviderInfoByDescription()
        {
            List<GeneralParameter> returnData = null;
            using (var db = RepositoryBase.GetA2isHealthDB())
            {
                returnData = db.Fetch<GeneralParameter>(QueryCollection.qGetProviderInfoByDesc);
            }

            return returnData;
        }

        internal static string GetMedcareClaimID()
        {
            using (var db = RepositoryBase.GetA2isHealthDB())
            {
                return db.Fetch<string>
                    (
                        QueryCollection.QueryGetMedcareClaimID
                    ).FirstOrDefault();

            }
        }

        internal static string GetReimbursementClaimID(string claimNo)
        {
            using (var db = RepositoryBase.GetA2isHealthDB())
            {
                return db.Fetch<string>
                    (
                        QueryCollection.QueryGetReimbursementClaimID,
                        claimNo
                    ).FirstOrDefault() ?? claimNo;
            }
        }
    }
}